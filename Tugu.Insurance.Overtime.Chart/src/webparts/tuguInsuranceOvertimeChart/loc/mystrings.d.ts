declare interface ITuguInsuranceOvertimeChartWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  LoadingSpinnerLabel: string;
  ErrorLabel: "Error";
  WarningLabel: "Warning";
}

declare module 'TuguInsuranceOvertimeChartWebPartStrings' {
  const strings: ITuguInsuranceOvertimeChartWebPartStrings;
  export = strings;
}
