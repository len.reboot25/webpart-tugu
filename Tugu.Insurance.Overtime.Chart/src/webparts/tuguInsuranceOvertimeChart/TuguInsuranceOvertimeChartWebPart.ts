import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'TuguInsuranceOvertimeChartWebPartStrings';
import TuguInsuranceOvertimeChart from './components/TuguInsuranceOvertimeChart';
import { ITuguInsuranceOvertimeChartProps } from './components/ITuguInsuranceOvertimeChartProps';

import { PropertyFieldListPicker, PropertyFieldListPickerOrderBy } from '@pnp/spfx-property-controls/lib/PropertyFieldListPicker';
import { PropertyFieldColumnPicker, PropertyFieldColumnPickerOrderBy, IColumnReturnProperty } from '@pnp/spfx-property-controls/lib/PropertyFieldColumnPicker';
import { setup as pnpSetup } from '@pnp/common';
import { PropertyFieldColorPicker, PropertyFieldColorPickerStyle } from '@pnp/spfx-property-controls/lib/PropertyFieldColorPicker';


export interface ITuguInsuranceOvertimeChartWebPartProps {
  title: string;
  listidChartByGroup: string;
  keyChartByGroup: string;
  filterMonthChartByGroup: string;
  filterYearChartByGroup: string;
  labelChartByGroup: string;
  datasetChartByGroup: string;

  listidChartByPersonal: string;
  keyChartByPersonal: string;
  filterMonthChartByPersonal: string;
  filterYearChartByPersonal: string;
  labelChartByPersonal: string;
  datasetChartByPersonal: string;
  listidGroup: string;
  keyGroup: string;
  valueGroup: string;

  chartColour: string;
}

export default class TuguInsuranceOvertimeChartWebPart extends BaseClientSideWebPart<ITuguInsuranceOvertimeChartWebPartProps> {

  public render(): void {
    const element: React.ReactElement<ITuguInsuranceOvertimeChartProps> = React.createElement(
      TuguInsuranceOvertimeChart,
      {
        title: this.properties.title,
        needsConfiguration: this._needsConfiguration(),
        configureHandler: this._onConfigure,
        _context: this.context,
        displayMode: this.displayMode,
        onTitleUpdate: (newTitle: string) => {
          // after updating the web part title in the component
          // persist it in web part properties yes
          this.properties.title = newTitle;
        },

        listidChartByGroup: this.properties.listidChartByGroup,
        keyChartByGroup: this.properties.keyChartByGroup,
        filterMonthChartByGroup: this.properties.filterMonthChartByGroup,
        filterYearChartByGroup: this.properties.filterYearChartByGroup,
        labelChartByGroup: this.properties.labelChartByGroup,
        datasetChartByGroup: this.properties.datasetChartByGroup,

        listidChartByPersonal: this.properties.listidChartByPersonal,
        keyChartByPersonal: this.properties.keyChartByPersonal,
        filterMonthChartByPersonal: this.properties.filterMonthChartByPersonal,
        filterYearChartByPersonal: this.properties.filterYearChartByPersonal,
        labelChartByPersonal: this.properties.labelChartByPersonal,
        datasetChartByPersonal: this.properties.datasetChartByPersonal,
        listidGroup: this.properties.listidGroup,
        keyGroup: this.properties.keyGroup,
        valueGroup: this.properties.valueGroup,
        chartColour: this.properties.chartColour
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  //protected get dataVersion(): Version {
    //return Version.parse('1.0');
  //}

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          displayGroupsAsAccordion: true,
          groups: [
            {
              groupName: "Source Data Chart By Group",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidChartByGroup', {
                  label: 'Select a list',
                  selectedList: this.properties.listidChartByGroup,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyChartByGroup', {
                  label: 'Select columns for Group Code',
                  context: this.context as any,
                  selectedColumn: this.properties.keyChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterMonthChartByGroup', {
                  label: 'Select columns for filter month',
                  context: this.context as any,
                  selectedColumn: this.properties.filterMonthChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterYearChartByGroup', {
                  label: 'Select columns for filter year',
                  context: this.context as any,
                  selectedColumn: this.properties.filterYearChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('labelChartByGroup', {
                  label: 'Select columns for label chart',
                  context: this.context as any,
                  selectedColumn: this.properties.labelChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('datasetChartByGroup', {
                  label: 'Select columns for dataset chart',
                  context: this.context as any,
                  selectedColumn: this.properties.datasetChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                })
              ]
            },
            {
              groupName: "Source Data Chart By Person",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidChartByPersonal', {
                  label: 'Select a list',
                  selectedList: this.properties.listidChartByPersonal,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyChartByPersonal', {
                  label: 'Select columns for Group Code',
                  context: this.context as any,
                  selectedColumn: this.properties.keyChartByPersonal,
                  listId: this.properties.listidChartByPersonal,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterMonthChartByPersonal', {
                  label: 'Select columns for filter month',
                  context: this.context as any,
                  selectedColumn: this.properties.filterMonthChartByPersonal,
                  listId: this.properties.listidChartByPersonal,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterYearChartByPersonal', {
                  label: 'Select columns for filter year',
                  context: this.context as any,
                  selectedColumn: this.properties.filterYearChartByPersonal,
                  listId: this.properties.listidChartByPersonal,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('labelChartByPersonal', {
                  label: 'Select columns for label chart',
                  context: this.context as any,
                  selectedColumn: this.properties.labelChartByPersonal,
                  listId: this.properties.listidChartByPersonal,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('datasetChartByPersonal', {
                  label: 'Select columns for dataset chart',
                  context: this.context as any,
                  selectedColumn: this.properties.datasetChartByPersonal,
                  listId: this.properties.listidChartByPersonal,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                })
              ]
            },
            {
              groupName: "Source Data Group",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidGroup', {
                  label: 'Select a list',
                  selectedList: this.properties.listidGroup,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyGroup', {
                  label: 'Select columns for key',
                  context: this.context as any,
                  selectedColumn: this.properties.keyGroup,
                  listId: this.properties.listidGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('valueGroup', {
                  label: 'Select columns for value',
                  context: this.context as any,
                  selectedColumn: this.properties.valueGroup,
                  listId: this.properties.listidGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                })
              ]
            },
            {
              groupName: "Personalize Settings",
                isCollapsed: true,
                groupFields: [
                PropertyFieldColorPicker('chartColour', {
                    label: 'Color Chart',
                    selectedColor: this.properties.chartColour,
                    onPropertyChange: this.onPropertyPaneFieldChanged,
                    properties: this.properties,
                    disabled: false,
                    debounce: 1000,
                    isHidden: false,
                    alphaSliderHidden: false,
                    style: PropertyFieldColorPickerStyle.Inline,
                    iconName: 'Precipitation',
                    key: 'colorFieldId'
                })
              ]
            }
          ]
        }
      ]
    };
  }

  protected onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnpSetup({
        spfxContext: this.context
      });
    });
  }

  /**
   * Handles clicking the Configure button in the placeholder
   */
  private _onConfigure = (): void => {
    // open the property pane to let the user configure the web part
    this.context.propertyPane.open();
  }

   /**
   * Check if the web part has been configured
   */
  private _needsConfiguration(): boolean {
    return (this.properties.listidChartByGroup == "" || typeof(this.properties.listidChartByGroup) == "undefined")
    && (this.properties.listidChartByPersonal == "" || typeof(this.properties.listidChartByPersonal) == "undefined")
    && (this.properties.listidGroup == "" || typeof(this.properties.listidGroup) == "undefined")
    ;
  }
}
