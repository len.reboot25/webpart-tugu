import { WebPartContext } from "@microsoft/sp-webpart-base";
import { DisplayMode } from "@microsoft/sp-core-library";

export interface ITuguInsuranceOvertimeChartProps {
  title: string;

  needsConfiguration: boolean;

  configureHandler: () => void;
  
  _context: WebPartContext;

  /**
   * Current page display mode. Used to determine if the user should
   * be able to edit the page title or not.
   */
   displayMode: DisplayMode;

   /**
   * Event handler for changing the web part title
   */
  onTitleUpdate: (newTitle: string) => void;

  listidChartByGroup: string;
  keyChartByGroup: string;
  filterMonthChartByGroup: string;
  filterYearChartByGroup: string;
  labelChartByGroup: string;
  datasetChartByGroup: string;

  listidChartByPersonal: string;
  keyChartByPersonal: string;
  filterMonthChartByPersonal: string;
  filterYearChartByPersonal: string;
  labelChartByPersonal: string;
  datasetChartByPersonal: string;
  listidGroup: string;
  keyGroup: string;
  valueGroup: string;

  chartColour: string;
}
