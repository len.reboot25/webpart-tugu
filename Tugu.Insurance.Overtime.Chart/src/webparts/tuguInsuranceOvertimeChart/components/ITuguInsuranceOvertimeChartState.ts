export interface ITuguInsuranceOvertimeChartState {
  title: string;
  
  /**
   * True if the component is loading its data, false otherwise
   */
   loading: boolean;

   /**
    * error message 
    */
   errorMessage: string;

   warningMessage: string;

   content: JSX.Element;

   groupOptions: any[];

   periodOptions: any[];

   defaultValueGroup: string;

   defaultValuePeriod: string;
}
