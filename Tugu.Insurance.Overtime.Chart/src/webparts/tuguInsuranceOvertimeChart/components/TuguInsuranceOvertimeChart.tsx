import * as React from 'react';
import styles from './TuguInsuranceOvertimeChart.module.scss';
import { ITuguInsuranceOvertimeChartProps } from './ITuguInsuranceOvertimeChartProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { spfi, SPFI, SPFx } from "@pnp/sp";
import { getSP } from "../pnpjsConfig";
import {Dropdown, PrimaryButton, IDropdownOption} from '@fluentui/react';

import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

import {
  MessageBar,
  MessageBarType
} from 'office-ui-fabric-react/lib/MessageBar';

import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";

import { ChartControl, ChartType } from '@pnp/spfx-controls-react/lib/ChartControl';
import { SPPermission } from '@microsoft/sp-page-context'; 
import { IDropdownStyles } from 'office-ui-fabric-react';
import { ITuguInsuranceOvertimeChartState } from './ITuguInsuranceOvertimeChartState';
import * as strings from 'TuguInsuranceOvertimeChartWebPartStrings';

/**
 * Pre requisite development
 * NVM Version: 10.17.0
 * Typescript version: @microsoft/rush-stack-compiler-4.2
 * pnp version: 3.6.0
 */

const dropdownStyles: Partial<IDropdownStyles> = {
  dropdown: { width: "30%" },
};

const _month : any = ["January","February","March","April","May","June","July","August","September","October","November","December"];

export default class TuguInsuranceOvertimeChart extends React.Component<ITuguInsuranceOvertimeChartProps, ITuguInsuranceOvertimeChartState> {
  private _sp: SPFI;

  constructor(props: ITuguInsuranceOvertimeChartProps, state : ITuguInsuranceOvertimeChartState)
  {
    super(props);
    this.state = {
      title: this.props.title,
      loading: true,
      errorMessage: null,
      warningMessage: null,
      content: null,
      groupOptions: [],
      periodOptions: [],
      defaultValueGroup: null,
      defaultValuePeriod: null
    };
  }

  public render(): React.ReactElement<ITuguInsuranceOvertimeChartProps> {
    let contents: JSX.Element;
    
    if (this.props.needsConfiguration === false) {
      if (this.state.loading) {
        // Component is loading its data. Show spinner to communicate this to the user
        contents = <Spinner size={SpinnerSize.large} label={strings.LoadingSpinnerLabel} />;
      } if(!this.state.loading && this.state.errorMessage) {
        contents = <MessageBar
            messageBarType={MessageBarType.error}
            isMultiline={true}>{strings.ErrorLabel}: {this.state.errorMessage}</MessageBar>
      } if(!this.state.loading && this.state.warningMessage) {
        contents = <MessageBar
          messageBarType={MessageBarType.warning}
          isMultiline={true}>{strings.WarningLabel}: {this.state.warningMessage}</MessageBar>
      }else {
        contents = (
          this.state.content
        );
      }
    }
    
    let _permission = this.props._context.pageContext.web.permissions;
    let canEdit = _permission.hasPermission(SPPermission.manageWeb);
    
    return (
      <div className={styles.tuguInsuranceOvertimeChart}>
        <WebPartTitle
        displayMode={this.props.displayMode}
        title={this.props.title}
        updateProperty={this.props.onTitleUpdate} 
        />
        <Dropdown
            placeholder="Select period data"
            selectedKey={this.state.defaultValuePeriod}
            defaultSelectedKey={(new Date().getMonth()+1)+"-"+new Date().getFullYear()}
            label="Select Period"
            options={this.state.periodOptions}
            styles={dropdownStyles}
            onChange={this.onPeriodChange}
          />
        {
          canEdit && this.state.groupOptions &&
          <Dropdown
            placeholder="Select data to be display"
            selectedKey={this.state.defaultValueGroup}
            defaultSelectedKey={"--me--"}
            label="Select Group or My Data"
            options={this.state.groupOptions}
            styles={dropdownStyles}
            onChange={this.onDataChange}
          />
        }
        <br />
        {
          this.props.needsConfiguration &&
          
          <Placeholder
              iconName='Edit'
              iconText='Tugu Insurance - Overtime Chart Webpart'
              description='Configure your web part'
              buttonLabel='Configure'
              onConfigure={this.props.configureHandler} />
        }
        {
          !this.props.needsConfiguration &&
          contents
        }
      </div>
    );
  }

  public componentDidUpdate(prevProps, prevState): void {
    if(prevState.defaultValuePeriod != this.state.defaultValuePeriod || prevState.defaultValueGroup != this.state.defaultValueGroup){
      this.setState({ loading : true, warningMessage : null, errorMessage : null, content : null});
      let _periodMonth = String(new Date().getMonth()+1);
      let _periodYear = String(new Date().getFullYear());
      
      let _group = "--me--";
      
      if(this.state.defaultValueGroup != null)
        _group = this.state.defaultValueGroup;
      if(this.state.defaultValuePeriod != null){
        let _period = this.state.defaultValuePeriod.split("-");
        _periodMonth = _period[0];
        _periodYear = _period[1];
      }

      if(_group == "--me--"){
        this._loadReportPeronal(_periodMonth, _periodYear);
        this.forceUpdate();
      } else {
        this._loadReportGroup(_group, _periodMonth, _periodYear);
        this.forceUpdate();
      }
    }
  }

  public componentDidMount(): void {
    
    if (!this.props.needsConfiguration) {
      // The web part has been configured. Load the chart
      this._loadOptionsGroup();
      this._loadOptionsPeriod();
      
      let _filterMonth =  new Date().getMonth() + 1;
      let _filterYear =  new Date().getFullYear();
      this._loadReportPeronal(_filterMonth, _filterYear);
    }
  }

  private _loadReportGroup(_groupCode, _filterMonth, _filterYear){
    this.setState({ loading : false });
    try
    {
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidChartByGroup)
      .items
      .select(this.props.labelChartByGroup, this.props.datasetChartByGroup
        )
      .filter(this.props.keyChartByGroup+" eq '"+_groupCode+"' and "+
      this.props.filterMonthChartByGroup+ " eq '"+_filterMonth+"' and "+
      this.props.filterYearChartByGroup+ " eq '"+_filterYear+"'"
      )()
      .then((items: any[]) => {
        if(items.length > 0){
          var _labels = [], _dataset = [];

          for(var i = 0; i < items.length; i++){
            _labels.push(items[i][this.props.labelChartByGroup]);            
            _dataset.push(items[i][this.props.datasetChartByGroup]);
          }

          const data: any = {
              labels:
              _labels,
              datasets: [
                {
                  fill: false,
                  data:
                    _dataset,
                    borderWidth: 1,
                    tension: 0.1,
                    backgroundColor: this.props.chartColour == "" ? "rgb(75, 192, 192)" : this.props.chartColour,
                    borderColor: this.props.chartColour == "" ? "rgb(75, 192, 192)" : this.props.chartColour
                }
              ]
          };
          
          // set the options
          const options: any = {
            legend: {
              display: false,
            }
          };
          
          this.setState({ content :  <ChartControl
            type={ChartType.Line}
            data={data}
            options={options}
          />});
        } else {
          this.setState({ warningMessage : "There is no data" });
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    } 
  }

  private _loadReportPeronal(_filterMonth, _filterYear){
    this.setState({ loading : false });
    try
    {
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidChartByPersonal)
      .items
      .select(this.props.labelChartByPersonal, this.props.datasetChartByPersonal
        )
      .filter(this.props.keyChartByPersonal+" eq '"+this.props._context.pageContext.user.email+"' and "+
      this.props.filterMonthChartByPersonal+ " eq '"+_filterMonth+"' and "+
      this.props.filterYearChartByPersonal+ " eq '"+_filterYear+"'"
      )()
      .then((items: any[]) => {
        if(items.length > 0){
          var _labels = [], _dataset = [];

          for(var i = 0; i < items.length; i++){
            _labels.push(items[i][this.props.labelChartByPersonal]);            
            _dataset.push(items[i][this.props.datasetChartByPersonal]);
          }

          const data: any = {
              labels:
                _labels,
              datasets: [
                {
                  fill: false,
                  data:
                    _dataset,
                    borderWidth: 1,
                    tension: 0.1,
                    backgroundColor: this.props.chartColour == "" ? "rgb(75, 192, 192)" : this.props.chartColour,
                    borderColor: this.props.chartColour == "" ? "rgb(75, 192, 192)" : this.props.chartColour
                }
              ]
          };
          
          // set the options
          const options: any = {
            legend: {
              display: false,
            },
            scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
            } 
          };
          
          this.setState({ content :  <ChartControl
            type={ChartType.Line}
            data={data}
            options={options}
          />});
        } else {
          this.setState({ warningMessage : "There is no data" });
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    } 
  }

  private _loadOptionsGroup(){
    try
    {
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidGroup)
      .items
      .select(this.props.keyGroup, this.props.valueGroup)
      ()
      .then((items: any[]) => {
        let resultarr = [];
        resultarr.push({
          key:"--me--",
          text:"My Data"})

        if(items.length > 0){          
          for(var i = 0; i < items.length; i++){
            resultarr.push({
              key: items[i][this.props.keyGroup],
              text:items[i][this.props.valueGroup]})
          }                    
          this.setState({groupOptions : resultarr});
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    }
  }

  private _loadOptionsPeriod(){
    let resultarr = [];
    let i = 1;
    let currentMonth = new Date().getMonth()+1;
    let currentYear = new Date().getFullYear();
    while(i < 4){
      if((currentMonth - 1) == 0){
        currentMonth = 12;
        currentYear = currentYear-1;
      } 
      
      resultarr.push({
        key: currentMonth+"-"+currentYear,
        text: _month[currentMonth - 1] +" " +currentYear,
      })

      currentMonth = currentMonth - 1;
      i++;
    }

    this.setState({periodOptions : resultarr});
  }

  public onPeriodChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ defaultValuePeriod: item.key as string});
  }

  public onDataChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ defaultValueGroup: item.key as string});
  }
}
