import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

export interface ILayoutProps {
    labelQuota: string;
    valueQuota: string;
    labelUsage: string;
    valueUsage: string;
    labelRemaining: string;
    valueRemaining: string;
    _context: any;
    filePickerResultQuota: IFilePickerResult;
    filePickerResultUsage: IFilePickerResult;
    filePickerResultBalance: IFilePickerResult;
    filePickerBackground: IFilePickerResult;
}