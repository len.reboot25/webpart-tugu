import * as React from 'react';
import { ILayoutProps, ILayoutState } from '.';
import styles from './ILayout.module.scss';
import * as strings from 'TuguInsuranceHealthBenefitWebPartStrings';

export class Layoutcontent extends React.Component<ILayoutProps, ILayoutState> {
  constructor(props: ILayoutProps) {
    super(props);

    this.state = {

    };
  }

  public render(): React.ReactElement<ILayoutProps> {
    let { _context, labelQuota, valueQuota, labelUsage, valueUsage, labelRemaining, valueRemaining } = this.props;

    if(!isNaN(parseFloat(valueQuota)))
      valueQuota = "Rp."+ valueQuota.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if(!isNaN(parseFloat(valueUsage)))
      valueUsage = "Rp."+ valueUsage.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      
    if(!isNaN(parseFloat(valueRemaining)))
      valueRemaining = "Rp."+ valueRemaining.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    
    return (
        <div className={styles.content}>
          <table className={styles.healthBenefitContent} >
            <tr>
              <td className={styles.headerContent}><img className={styles.icon} src={this.props.filePickerResultQuota.fileAbsoluteUrl} /></td>
              <td><b>{labelQuota}</b></td>
            </tr>
            <tr>
              <td></td>
              <td className={styles.rowContent}>{valueQuota}</td>
            </tr>
            <tr>
              <td className={styles.headerContent}><img className={styles.icon} src={this.props.filePickerResultUsage.fileAbsoluteUrl} /></td>
              <td><b>{labelUsage}</b></td>
            </tr>
            <tr>
              <td></td>
              <td className={styles.rowUsage}>{valueUsage}</td>
            </tr>
            <tr>
              <td className={styles.headerContent}><img className={styles.icon} src={this.props.filePickerResultBalance.fileAbsoluteUrl} /></td>
              <td><b>{labelRemaining}</b></td>
            </tr>
            <tr>
              <td></td>
              <td className={styles.rowContent}>{valueRemaining}</td>
            </tr>
          </table>
        </div>
    );
  }
}
