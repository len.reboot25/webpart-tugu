import { WebPartContext } from "@microsoft/sp-webpart-base";
import { DisplayMode } from "@microsoft/sp-core-library";
import { IPropertyFieldSite, PropertyFieldSitePicker } from '@pnp/spfx-property-controls/lib/PropertyFieldSitePicker';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

export interface ITuguInsuranceHealthBenefitProps {
  title: string;

  needsConfiguration: boolean;

  configureHandler: () => void;
  
  _context: WebPartContext;

  /**
   * Current page display mode. Used to determine if the user should
   * be able to edit the page title or not.
   */
   displayMode: DisplayMode;

   /**
   * Event handler for changing the web part title
   */
  onTitleUpdate: (newTitle: string) => void;

  sites: IPropertyFieldSite[];

  site: string;
  
  listid: string;

  emailColumn: string;
  
  quota: string;
  
  quotaColumn: string;
  
  usageAmount: string;
  
  usageAmountColumn: string;
  
  balanceAmount: string;
  
  balanceAmountColumn: string;
  
  filePickerResultQuota: IFilePickerResult;
  filePickerResultUsage: IFilePickerResult;
  filePickerResultBalance: IFilePickerResult;
  filePickerBackground: IFilePickerResult;
  
}
