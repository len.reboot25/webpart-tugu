export interface ITuguInsuranceHealthBenefitState {
  title: string;

  /**
   * True if the component is loading its data, false otherwise
   */
   loading: boolean;

   /**
    * error message 
    */
   errorMessage: string;

   /**
    * warning message 
    */
    warningMessage: string;

   content: JSX.Element;
}
