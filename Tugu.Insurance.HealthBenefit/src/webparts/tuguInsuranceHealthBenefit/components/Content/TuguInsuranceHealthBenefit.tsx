import * as React from 'react';
import styles from './TuguInsuranceHealthBenefit.module.scss';
import { ITuguInsuranceHealthBenefitProps } from './ITuguInsuranceHealthBenefitProps';
import { ITuguInsuranceHealthBenefitState } from './ITuguInsuranceHealthBenefitState';
import { escape } from '@microsoft/sp-lodash-subset';
import { IConfigOptions } from '@pnp/common';

import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { spfi, SPFI, SPFx } from "@pnp/sp";
import { getSP } from "../../pnpjsConfig";
import { IItems } from "../../IItems";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Layoutcontent } from '../Layout/Layoutcontent';

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

import {
  MessageBar,
  MessageBarType
} from 'office-ui-fabric-react/lib/MessageBar';

import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";
import * as strings from 'TuguInsuranceHealthBenefitWebPartStrings';
import { Link } from 'office-ui-fabric-react';

/**
 * Pre requisite development
 * NVM Version: 10.17.0
 * Typescript version: @microsoft/rush-stack-compiler-4.2
 * pnp version: 3.6.0
 */

export default class TuguInsuranceHealthBenefit extends React.Component<ITuguInsuranceHealthBenefitProps, ITuguInsuranceHealthBenefitState> {
  private _sp: SPFI;
  
  constructor(props: ITuguInsuranceHealthBenefitProps, state : ITuguInsuranceHealthBenefitState)
  {
    super(props);
    this.state = {
      title: this.props.title,
      loading: true,
      errorMessage: null,
      warningMessage: null,
      content: null
    };

    
  }

  public render(): React.ReactElement<ITuguInsuranceHealthBenefitProps> {
    let contents: JSX.Element;
    
    if (this.props.needsConfiguration === false) {
      if (this.state.loading) {
        // Component is loading its data. Show spinner to communicate this to the user
        contents = <Spinner size={SpinnerSize.large} label={strings.LoadingSpinnerLabel} />;
      } if(!this.state.loading && this.state.errorMessage) {
        contents = <MessageBar
            messageBarType={MessageBarType.error}
            isMultiline={false}>{strings.ErrorLabel}: {this.state.errorMessage}</MessageBar>
      } if(!this.state.loading && this.state.warningMessage) {
        contents = <MessageBar
            messageBarType={MessageBarType.warning}
            isMultiline={false}>{strings.WarningLabel}: {this.state.warningMessage}</MessageBar>
      } else {
        contents = (
          this.state.content
        );
      }
    }

    let backgroundImg = '';
    if(typeof(this.props.filePickerBackground) != "undefined")
      backgroundImg = this.props.filePickerBackground.fileAbsoluteUrl;

      
    return (
      <div className={styles.tuguInsuranceHealthBenefit} style={{backgroundImage:'url("'+ backgroundImg +'")'}}>
        <WebPartTitle
        displayMode={this.props.displayMode}
        title={this.props.title}
        updateProperty={this.props.onTitleUpdate} 
        className={styles.webpartTitle} 
        />

        {
          this.props.needsConfiguration &&
          
          <Placeholder
              iconName='Edit'
              iconText='Tugu Insurance - Health Benefit Webpart'
              description='Configure your web part'
              buttonLabel='Configure'
              onConfigure={this.props.configureHandler} />
        }
        {
          !this.props.needsConfiguration &&
          contents
        }
      </div>
    );
  }

  public componentDidMount(): void {
    if (!this.props.needsConfiguration) {
      this.setState({ loading : false });
      this._loadData();
    }
  }

  private async _loadData(){
    try
    {
      /**
       * reference
       * https://github.com/pnp/sp-dev-fx-webparts/tree/main/samples/react-pnp-js-sample
       */

      /** 
       * this._sp = getSP(this.props.sites[0]["url"], this.props._context);
       * if use site picker
       * */ 
      
      this._sp = getSP(this.props.site, this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listid)
      .items
      .select(this.props.quotaColumn, this.props.usageAmountColumn, this.props.balanceAmountColumn)
      .top(1).filter(this.props.emailColumn+" eq '"+this.props._context.pageContext.user.email+"'")()
      .then((items: IItems[]) => {
        if(items.length > 0){
          this.setState({ content : <Layoutcontent 
            labelQuota={this.props.quota} 
            labelUsage={this.props.usageAmount}
            labelRemaining={this.props.balanceAmount}
            valueQuota={items[0][this.props.quotaColumn]}
            valueUsage={items[0][this.props.usageAmountColumn]}
            valueRemaining={items[0][this.props.balanceAmountColumn]}
            _context={this.props._context}
            filePickerResultQuota={this.props.filePickerResultQuota}
            filePickerResultUsage={this.props.filePickerResultUsage}
            filePickerResultBalance={this.props.filePickerResultBalance}
            filePickerBackground={this.props.filePickerBackground}
            /> 
          
          })
        } else {
          this.setState({ warningMessage : 'There is any data for current user login, please check data source/list'});
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    }
  }
}
