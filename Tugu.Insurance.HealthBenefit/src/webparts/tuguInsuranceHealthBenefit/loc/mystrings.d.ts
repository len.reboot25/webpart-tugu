declare interface ITuguInsuranceHealthBenefitWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  LoadingSpinnerLabel: string;
  ErrorLabel: "Error";
  WarningLabel: "Warning";
}

declare module 'TuguInsuranceHealthBenefitWebPartStrings' {
  const strings: ITuguInsuranceHealthBenefitWebPartStrings;
  export = strings;
}
