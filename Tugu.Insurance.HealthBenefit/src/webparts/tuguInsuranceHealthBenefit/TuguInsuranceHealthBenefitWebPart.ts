import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'TuguInsuranceHealthBenefitWebPartStrings';
import TuguInsuranceHealthBenefit from './components/Content/TuguInsuranceHealthBenefit';
import { ITuguInsuranceHealthBenefitProps } from './components/Content/ITuguInsuranceHealthBenefitProps';
import { PropertyFieldListPicker, PropertyFieldListPickerOrderBy } from '@pnp/spfx-property-controls/lib/PropertyFieldListPicker';
import { PropertyFieldColumnPicker, PropertyFieldColumnPickerOrderBy, IColumnReturnProperty } from '@pnp/spfx-property-controls/lib/PropertyFieldColumnPicker';
import { PropertyFieldChoiceGroupWithCallout } from '@pnp/spfx-property-controls/lib/PropertyFieldChoiceGroupWithCallout';
import { IPropertyFieldSite, PropertyFieldSitePicker } from '@pnp/spfx-property-controls/lib/PropertyFieldSitePicker';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

import { setup as pnpSetup } from '@pnp/common';

export interface ITuguInsuranceHealthBenefitWebPartProps {
  title: string;
  sites: IPropertyFieldSite[];
  site: string;
  listid: string;
  emailColumn: string;
  quota: string;
  quotaColumn: string;
  usageAmount: string;
  usageAmountColumn: string;
  balanceAmount: string;
  balanceAmountColumn: string;
  filePickerResultQuota: IFilePickerResult;
  filePickerResultUsage: IFilePickerResult;
  filePickerResultBalance: IFilePickerResult;
  filePickerBackground: IFilePickerResult;
}

export default class TuguInsuranceHealthBenefitWebPart extends BaseClientSideWebPart<ITuguInsuranceHealthBenefitWebPartProps> {

  public render(): void {
    const element: React.ReactElement<ITuguInsuranceHealthBenefitProps> = React.createElement(
      TuguInsuranceHealthBenefit,
      {
        title: this.properties.title,
        needsConfiguration: this._needsConfiguration(),
        configureHandler: this._onConfigure,
        _context: this.context,
        displayMode: this.displayMode,
        onTitleUpdate: (newTitle: string) => {
          // after updating the web part title in the component
          // persist it in web part properties yes
          this.properties.title = newTitle;
        },
        sites: this.properties.sites,
        site: this.properties.site,
        listid: this.properties.listid,
        emailColumn: this.properties.emailColumn,
        quota: this.properties.quota,
        quotaColumn: this.properties.quotaColumn,
        usageAmount: this.properties.usageAmount,
        usageAmountColumn: this.properties.usageAmountColumn,
        balanceAmount: this.properties.balanceAmount,
        balanceAmountColumn: this.properties.balanceAmountColumn,
        filePickerResultQuota: this.properties.filePickerResultQuota,
        filePickerResultUsage: this.properties.filePickerResultUsage,
        filePickerResultBalance: this.properties.filePickerResultBalance,
        filePickerBackground: this.properties.filePickerBackground
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  //protected get dataVersion(): Version {
    //return Version.parse('1.0');
  //}

  protected onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnpSetup({
        spfxContext: this.context
      });
    });
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          displayGroupsAsAccordion: true,
          groups: [
            
            {
              groupName: "Source Data",
              groupFields: [
                PropertyFieldListPicker('listid', {
                  label: 'Select a list',
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  selectedList: this.properties.listid,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('emailColumn', {
                  label: 'Select columns for email employee',
                  context: this.context as any,
                  selectedColumn: this.properties.emailColumn,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('quotaColumn', {
                  label: 'Select columns for quota amount',
                  context: this.context as any,
                  selectedColumn: this.properties.quotaColumn,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('usageAmountColumn', {
                  label: 'Select columns as usage amount',
                  context: this.context as any,
                  selectedColumn: this.properties.usageAmountColumn,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('balanceAmountColumn', {
                  label: 'Select columns as balance remaining amount',
                  context: this.context as any,
                  selectedColumn: this.properties.balanceAmountColumn,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }), 
              ]
            },
            {
              groupName: 'Personalize',
              isCollapsed: true,
              groupFields: [
                PropertyPaneTextField('quota', {
                  label: "Label for quota"
                }),
                PropertyPaneTextField('usageAmount', {
                  label: "Label for usage amount"
                }),
                PropertyPaneTextField('balanceAmount', {
                  label: "Label for balance remaining amount"
                }),
                PropertyFieldFilePicker('filePickerResultQuota', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerResultQuota,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultQuota = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultQuota = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Browse icon Quota",                  
                }),
                PropertyFieldFilePicker('filePickerResultUsage', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerResultUsage,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultUsage = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultUsage = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Browse icon Usage Amount",                  
                }),
                PropertyFieldFilePicker('filePickerResultBalance', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerResultBalance,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultBalance = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerResultBalance = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Browse icon Balance Remaining",                  
                }),
                PropertyFieldFilePicker('filePickerBackground', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerBackground,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Browse Background Image",                  
                })
              ]
            }
          ]
        }
      ]
    };
  }

  /**
   * Handles clicking the Configure button in the placeholder
   */
  private _onConfigure = (): void => {
    // open the property pane to let the user configure the web part
    this.context.propertyPane.open();
  }
  
  /**
   * Check if the web part has been configured
   */
   private _needsConfiguration(): boolean {
    return (this.properties.listid == "" || typeof(this.properties.listid) == "undefined");
  }
}
