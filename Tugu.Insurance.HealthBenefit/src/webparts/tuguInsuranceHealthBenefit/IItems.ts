export interface IItems {
    _Quota: string;
    _Usage: string;
    _Remaining: string;
}