import { WebPartContext } from "@microsoft/sp-webpart-base";
import { DisplayMode } from "@microsoft/sp-core-library";
import { IPropertyFieldSite, PropertyFieldSitePicker } from '@pnp/spfx-property-controls/lib/PropertyFieldSitePicker';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

export interface ILeaveBalanceTuguInsuranceProps {
  title: string;

  needsConfiguration: boolean;

  configureHandler: () => void;
  
  _context: WebPartContext;

  /**
   * Current page display mode. Used to determine if the user should
   * be able to edit the page title or not.
   */
   displayMode: DisplayMode;

   /**
   * Event handler for changing the web part title
   */
  onTitleUpdate: (newTitle: string) => void;

  sites: IPropertyFieldSite[];
  site: string;
  listid: string;
  emailColumn: string;
  labelPrevLeaveBalance: string;
  
  labelValidPrevLeaveBalance: string;
  ValidPrevLeaveBalance: string;

  labelPrevAnnual:string;
  prevAnnual: string;
  labelUsedPrevAnnual:string;
  usedPrevAnnual: string;

  labelPrevLongLeave:string;
  prevLongLeave: string;
  labelUsedPrevLongLeave:string;
  usedPrevLongLeave: string;
  
  labelPrevBalance: string;
  prevBalance: string;

  labelCurrLeaveBalance: string;
  labelValidCurrLeaveBalance: string;
  ValidCurrLeaveBalance: string;
  
  labelCurrAnnual: string;
  currAnnual: string;
  labelUsedCurrAnnual: string;
  usedCurrAnnual: string;

  labelCurrLongLeave: string;
  currLongLeave: string;
  labelUsedCurrLongLeave: string;
  usedCurrLongLeave: string;
  
  labelCurrBalance: string;
  currBalance: string;

  filePickerBackground: IFilePickerResult;
}
