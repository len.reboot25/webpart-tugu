import * as React from 'react';
import styles from './LeaveBalanceTuguInsurance.module.scss';
import { ILeaveBalanceTuguInsuranceProps } from './ILeaveBalanceTuguInsuranceProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { ILeaveBalanceTuguInsuranceState } from './ILeaveBalanceTuguInsuranceState';

import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { spfi, SPFI, SPFx } from "@pnp/sp";
import { getSP } from "../../pnpjsConfig";

import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Layoutcontent } from '../Layout/Layoutcontent';

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

import {
  MessageBar,
  MessageBarType
} from 'office-ui-fabric-react/lib/MessageBar';

import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";

import * as strings from 'LeaveBalanceTuguInsuranceWebPartStrings';

/**
 * Pre requisite development
 * NVM Version: 10.17.0
 * Typescript version: @microsoft/rush-stack-compiler-4.2
 * pnp version: 3.6.0
 */

export default class LeaveBalanceTuguInsurance extends React.Component<ILeaveBalanceTuguInsuranceProps, ILeaveBalanceTuguInsuranceState> {
  private _sp: SPFI;

  constructor(props: ILeaveBalanceTuguInsuranceProps, state : ILeaveBalanceTuguInsuranceState)
  {
    super(props);
    this.state = {
      title: this.props.title,
      loading: true,
      errorMessage: null,
      warningMessage: null,
      content: null
    };
  }

  public render(): React.ReactElement<ILeaveBalanceTuguInsuranceProps> {
    let contents: JSX.Element;

    if (this.props.needsConfiguration === false) {
      if (this.state.loading) {
        // Component is loading its data. Show spinner to communicate this to the user
        contents = <Spinner size={SpinnerSize.large} label={strings.LoadingSpinnerLabel} />;
      } if(!this.state.loading && this.state.errorMessage) {
        contents = <MessageBar
            messageBarType={MessageBarType.error}
            isMultiline={false}>{strings.ErrorLabel}: {this.state.errorMessage}</MessageBar>
      } if (!this.state.loading && this.state.warningMessage) {
        console.log("check");
        contents = <MessageBar
            messageBarType={MessageBarType.warning}
            isMultiline={false}>{strings.WarningLabel}: {this.state.warningMessage}</MessageBar>
      } else {
        contents = (
          this.state.content
        );
      }
    }

    let backgroundImg = '';
    if(typeof(this.props.filePickerBackground) != "undefined")
      backgroundImg = this.props.filePickerBackground.fileAbsoluteUrl;

    return (
      <div className={styles.leaveBalanceTuguInsurance}  style={{backgroundImage:'url("'+ backgroundImg +'")'}}>
        <WebPartTitle
        displayMode={this.props.displayMode}
        title={this.props.title}
        updateProperty={this.props.onTitleUpdate}
        className={styles.webpartTitle + "bold"}
        />

        {
          this.props.needsConfiguration &&

          <Placeholder
              iconName='Edit'
              iconText='Tugu Insurance - Leave Benefit Webpart'
              description='Configure your web part'
              buttonLabel='Configure'
              onConfigure={this.props.configureHandler} />
        }
        {
          !this.props.needsConfiguration &&
          contents
        }
      </div>
    );
  }

  public componentDidMount(): void {
    if (!this.props.needsConfiguration) {
      this.setState({ loading : false });
      this._loadData();
    }
  }

  private async _loadData(){
    try
    {
      /**
       * reference
       * https://github.com/pnp/sp-dev-fx-webparts/tree/main/samples/react-pnp-js-sample
       */

      /**
       * this._sp = getSP(this.props.sites[0]["url"], this.props._context);
       * if use site picker
       * */

      this._sp = getSP(this.props.site, this.props._context);

      this._sp.web.lists
      .getById(this.props.listid)
      .items
      .select(this.props.currAnnual, this.props.currBalance, this.props.currLongLeave, this.props.prevAnnual,
        this.props.prevBalance, this.props.prevLongLeave, this.props.usedCurrAnnual, this.props.usedCurrLongLeave, this.props.usedPrevAnnual, this.props.usedPrevLongLeave,
        this.props.ValidCurrLeaveBalance, this.props.ValidPrevLeaveBalance
        )
      .top(1).filter(this.props.emailColumn+" eq '"+this.props._context.pageContext.user.email+"'")()
      .then((items: any[]) => {
        if(items.length > 0){
          this.setState({ content : <Layoutcontent
            labelPrevLeaveBalance={this.props.labelPrevLeaveBalance}
            labelPrevAnnual={this.props.labelPrevAnnual}
            labelPrevLongLeave={this.props.labelPrevLongLeave}
            labelPrevBalance={this.props.labelPrevBalance}
            labelUsedPrevAnnual={this.props.labelUsedPrevAnnual}
            labelUsedPrevLongLeave={this.props.labelUsedPrevLongLeave}
            labelValidPrevLeaveBalance={this.props.labelValidPrevLeaveBalance}

            prevAnnual={items[0][this.props.prevAnnual]}
            usedPrevAnnual={items[0][this.props.usedPrevAnnual]}
            prevLongLeave={items[0][this.props.prevLongLeave]}
            usedPrevLongLeave={items[0][this.props.usedPrevLongLeave]}
            prevBalance={items[0][this.props.prevBalance]}
            ValidPrevLeaveBalance={items[0][this.props.ValidPrevLeaveBalance]}

            labelCurrLeaveBalance={this.props.labelCurrLeaveBalance}
            labelCurrAnnual={this.props.labelCurrAnnual}
            labelCurrLongLeave={this.props.labelCurrLongLeave}
            labelCurrBalance={this.props.labelCurrBalance}
            labelUsedCurrAnnual={this.props.labelUsedCurrAnnual}
            labelUsedCurrLongLeave={this.props.labelUsedCurrLongLeave}
            labelValidCurrLeaveBalance={this.props.labelValidCurrLeaveBalance}
            ValidCurrLeaveBalance={items[0][this.props.ValidCurrLeaveBalance]}

            currAnnual={items[0][this.props.currAnnual]}
            usedCurrAnnual={items[0][this.props.usedCurrAnnual]}
            currLongLeave={items[0][this.props.currLongLeave]}

            usedCurrLongLeave={items[0][this.props.usedCurrLongLeave]}
            currBalance={items[0][this.props.currBalance]}

            />
          })
        } else {
          this.setState({ errorMessage : 'There isn\'t any data currently for login user, please check with the administrator'});
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    }
  }
}
