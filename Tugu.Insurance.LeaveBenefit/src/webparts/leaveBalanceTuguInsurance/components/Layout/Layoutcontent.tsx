import * as React from 'react';
import { ILayoutProps, ILayoutState } from '.';
import styles from './ILayout.module.scss';
import * as strings from 'LeaveBalanceTuguInsuranceWebPartStrings';

export class Layoutcontent extends React.Component<ILayoutProps, ILayoutState> {
  constructor(props: ILayoutProps) {
    super(props);

    this.state = {

    };
  }

  public render(): React.ReactElement<ILayoutProps> {
    
    return (
        <div className={styles.content}>
          <label className={styles.labelLeaveBalance}>{this.props.labelPrevLeaveBalance}</label>
            <table className={styles.leaveBenefitContent}>
              <tr>
                <td className={styles.headerContent}>{this.props.labelPrevAnnual}</td>
                <td className={styles.headerContent}>{this.props.labelPrevLongLeave}</td>
                <td className={styles.headerContent}>{this.props.labelPrevBalance}</td>
              </tr>
              <tr>
                <td className={styles.rowContent}>{this.props.prevAnnual}</td>
                <td className={styles.rowContent}>{this.props.prevLongLeave}</td>
                <td className={styles.rowContent}>{this.props.prevBalance}</td>
              </tr>
              <tr>
                <td className={styles.headerContent}>{this.props.labelUsedPrevAnnual}</td>
                <td className={styles.headerContent}>{this.props.labelUsedPrevLongLeave}</td>
              </tr>
              <tr>
                <td className={styles.rowContent}>{this.props.usedPrevAnnual}</td>
                <td className={styles.rowContent}>{this.props.usedPrevLongLeave}</td>
              </tr>
            </table>
            
            <label className={styles.validUntil}>{this.props.labelValidPrevLeaveBalance} {this.props.ValidPrevLeaveBalance}</label>
          <br />
          <br />
          <label className={styles.labelLeaveBalance}>{this.props.labelCurrLeaveBalance}</label>
            <table className={styles.leaveBenefitContent}>
            <tr>
                <td className={styles.headerContent}>{this.props.labelCurrAnnual}</td>
                <td className={styles.headerContent}>{this.props.labelCurrLongLeave}</td>
                <td className={styles.headerContent}>{this.props.labelCurrBalance}</td>
              </tr>
              <tr>
                <td className={styles.rowContent}>{this.props.currAnnual}</td>
                <td className={styles.rowContent}>{this.props.currLongLeave}</td>
                <td className={styles.rowContent}>{this.props.currBalance}</td>
              </tr>
              <tr>
                <td className={styles.headerContent}>{this.props.labelUsedCurrAnnual}</td>
                <td className={styles.headerContent}>{this.props.labelUsedCurrLongLeave}</td>
              </tr>
              <tr>
                <td className={styles.rowContent}>{this.props.usedCurrAnnual}</td>
                <td className={styles.rowContent}>{this.props.usedCurrLongLeave}</td>
              </tr>
            </table>
            
            <label className={styles.validUntil}>{this.props.labelValidCurrLeaveBalance} {this.props.ValidCurrLeaveBalance}</label>
        </div>
    );
  }
}
