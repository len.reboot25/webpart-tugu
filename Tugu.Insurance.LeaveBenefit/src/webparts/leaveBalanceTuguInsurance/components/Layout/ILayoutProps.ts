export interface ILayoutProps {
    labelPrevLeaveBalance: string;
    
    labelPrevAnnual:string;
    prevAnnual: string;
    labelUsedPrevAnnual:string;
    usedPrevAnnual: string;

    labelPrevLongLeave:string;
    prevLongLeave: string;
    labelUsedPrevLongLeave:string;
    usedPrevLongLeave: string;
    
    labelPrevBalance: string;
    prevBalance: string;
    labelValidPrevLeaveBalance: string;
    ValidPrevLeaveBalance: string;

    labelCurrLeaveBalance: string;
    labelCurrAnnual: string;
    currAnnual: string;
    labelUsedCurrAnnual: string;
    usedCurrAnnual: string;
    
    labelCurrLongLeave: string;
    currLongLeave: string;
    labelUsedCurrLongLeave: string;
    usedCurrLongLeave: string;
    
    labelCurrBalance: string;
    currBalance: string;
    
    labelValidCurrLeaveBalance: string;
    ValidCurrLeaveBalance: string;
}