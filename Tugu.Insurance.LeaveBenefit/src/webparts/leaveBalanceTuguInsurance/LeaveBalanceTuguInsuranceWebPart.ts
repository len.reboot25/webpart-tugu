import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'LeaveBalanceTuguInsuranceWebPartStrings';
import LeaveBalanceTuguInsurance from './components/Content/LeaveBalanceTuguInsurance';
import { ILeaveBalanceTuguInsuranceProps } from './components/Content/ILeaveBalanceTuguInsuranceProps';
import { PropertyFieldListPicker, PropertyFieldListPickerOrderBy } from '@pnp/spfx-property-controls/lib/PropertyFieldListPicker';
import { PropertyFieldColumnPicker, PropertyFieldColumnPickerOrderBy, IColumnReturnProperty } from '@pnp/spfx-property-controls/lib/PropertyFieldColumnPicker';
import { IPropertyFieldSite, PropertyFieldSitePicker } from '@pnp/spfx-property-controls/lib/PropertyFieldSitePicker';
import { setup as pnpSetup } from '@pnp/common';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

export interface ILeaveBalanceTuguInsuranceWebPartProps {
  title: string;
  sites: IPropertyFieldSite[];
  site: string;
  listid: string;
  emailColumn: string;
  
  labelPrevLeaveBalance: string;
  labelValidPrevLeaveBalance: string;
  ValidPrevLeaveBalance: string;

  labelPrevAnnual:string;
  prevAnnual: string;
  labelUsedPrevAnnual:string;
  usedPrevAnnual: string;

  labelPrevLongLeave:string;
  prevLongLeave: string;
  labelUsedPrevLongLeave:string;
  usedPrevLongLeave: string;
  
  labelPrevBalance: string;
  prevBalance: string;

  labelCurrLeaveBalance: string;
  labelValidCurrLeaveBalance: string;
  ValidCurrLeaveBalance: string;

  labelCurrAnnual: string;
  currAnnual: string;
  labelUsedCurrAnnual: string;
  usedCurrAnnual: string;

  labelCurrLongLeave: string;
  currLongLeave: string;
  labelUsedCurrLongLeave: string;
  usedCurrLongLeave: string;
  
  labelCurrBalance: string;
  currBalance: string;
  
  filePickerBackground: IFilePickerResult;
}

export default class LeaveBalanceTuguInsuranceWebPart extends BaseClientSideWebPart<ILeaveBalanceTuguInsuranceWebPartProps> {

  public render(): void {
    const element: React.ReactElement<ILeaveBalanceTuguInsuranceProps> = React.createElement(
      LeaveBalanceTuguInsurance,
      {
        title: this.properties.title,
        needsConfiguration: this._needsConfiguration(),
        configureHandler: this._onConfigure,
        _context: this.context,
        displayMode: this.displayMode,
        onTitleUpdate: (newTitle: string) => {
          // after updating the web part title in the component
          // persist it in web part properties yes
          this.properties.title = newTitle;
        },

        sites: this.properties.sites,
        site: this.properties.site,
        listid: this.properties.listid,
        emailColumn: this.properties.emailColumn,
        labelPrevLeaveBalance: this.properties.labelPrevLeaveBalance,
        labelPrevAnnual:this.properties.labelPrevAnnual,
        prevAnnual: this.properties.prevAnnual,
        labelUsedPrevAnnual: this.properties.labelUsedPrevAnnual,
        usedPrevAnnual: this.properties.usedPrevAnnual,

        labelPrevLongLeave:this.properties.labelPrevLongLeave,
        prevLongLeave: this.properties.prevLongLeave,
        labelUsedPrevLongLeave: this.properties.labelUsedPrevLongLeave,
        usedPrevLongLeave: this.properties.usedPrevLongLeave,

        labelPrevBalance: this.properties.labelPrevBalance,
        prevBalance: this.properties.prevBalance,
        labelValidPrevLeaveBalance: this.properties.labelValidPrevLeaveBalance,
        ValidPrevLeaveBalance: this.properties.ValidPrevLeaveBalance,

        labelCurrLeaveBalance: this.properties.labelCurrLeaveBalance,
        labelCurrAnnual: this.properties.labelCurrAnnual,
        currAnnual: this.properties.currAnnual,
        labelUsedCurrAnnual: this.properties.labelUsedCurrAnnual,
        usedCurrAnnual: this.properties.usedCurrAnnual,

        labelCurrLongLeave: this.properties.labelCurrLongLeave,
        currLongLeave: this.properties.currLongLeave,
        labelUsedCurrLongLeave: this.properties.labelUsedCurrLongLeave,
        usedCurrLongLeave: this.properties.usedCurrLongLeave,

        labelCurrBalance: this.properties.labelCurrBalance,
        currBalance: this.properties.currBalance,
        labelValidCurrLeaveBalance: this.properties.labelValidCurrLeaveBalance,
        ValidCurrLeaveBalance: this.properties.ValidCurrLeaveBalance,
        filePickerBackground: this.properties.filePickerBackground
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  //protected get dataVersion(): Version {
    //return Version.parse('1.0');
  //}

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          displayGroupsAsAccordion: true,
          groups: [
            {
                groupName: "Source Data",
                isCollapsed: true,
                groupFields: [
                /*{
                PropertyPaneTextField('site', {
                  label: "Site url"
                })
                ,PropertyFieldSitePicker('sites', {
                  label: 'Select sites',
                  initialSites: this.properties.sites,
                  context: this.context as any,
                  deferredValidationTime: 500,
                  multiSelect: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'sitesFieldId'
                })
                */
                PropertyFieldListPicker('listid', {
                  label: 'Select a list',
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  selectedList: this.properties.listid,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                })
              ]
            },
            {
              groupName: 'Content Settings',
              isCollapsed: true,
              groupFields: [
                PropertyFieldColumnPicker('prevAnnual', {
                  label: 'Select columns for previous annual',
                  context: this.context as any,
                  selectedColumn: this.properties.prevAnnual,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('usedPrevAnnual', {
                  label: 'Select columns for previous used annual',
                  context: this.context as any,
                  selectedColumn: this.properties.usedPrevAnnual,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('prevLongLeave', {
                  label: 'Select columns as previous long leave',
                  context: this.context as any,
                  selectedColumn: this.properties.prevLongLeave,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('usedPrevLongLeave', {
                  label: 'Select columns for previous used long leave',
                  context: this.context as any,
                  selectedColumn: this.properties.usedPrevLongLeave,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('prevBalance', {
                  label: 'Select columns as previous balance leave',
                  context: this.context as any,
                  selectedColumn: this.properties.prevBalance,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('ValidPrevLeaveBalance', {
                  label: 'Select columns as valid previous leave',
                  context: this.context as any,
                  selectedColumn: this.properties.ValidPrevLeaveBalance,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),

                PropertyFieldColumnPicker('currAnnual', {
                  label: 'Select columns for current annual',
                  context: this.context as any,
                  selectedColumn: this.properties.currAnnual,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('usedCurrAnnual', {
                  label: 'Select columns for current used annual',
                  context: this.context as any,
                  selectedColumn: this.properties.usedCurrAnnual,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('currLongLeave', {
                  label: 'Select columns as current long leave',
                  context: this.context as any,
                  selectedColumn: this.properties.currLongLeave,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('usedCurrLongLeave', {
                  label: 'Select columns as current use long leave',
                  context: this.context as any,
                  selectedColumn: this.properties.usedCurrLongLeave,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('currBalance', {
                  label: 'Select columns as current balance leave',
                  context: this.context as any,
                  selectedColumn: this.properties.currBalance,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('ValidCurrLeaveBalance', {
                  label: 'Select columns as valid current leave',
                  context: this.context as any,
                  selectedColumn: this.properties.currBalance,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                })
              ]
            },
            {
              groupName: 'Personalize Settings',
              isCollapsed: true,
              groupFields: [
                PropertyFieldColumnPicker('emailColumn', {
                  label: 'Select columns for email employee',
                  context: this.context as any,
                  selectedColumn: this.properties.emailColumn,
                  //webAbsoluteUrl: typeof(this.properties.sites) == "undefined" ? "" : this.properties.sites[0]["url"],
                  //webAbsoluteUrl: this.properties.site,
                  listId: this.properties.listid,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyPaneTextField('labelPrevLeaveBalance', {
                  label: "Label for previous leave balance section"
                }),
                PropertyPaneTextField('labelPrevAnnual', {
                  label: "Label for previous annual leave"
                }),
                PropertyPaneTextField('labelUsedPrevAnnual', {
                  label: "Label for previous use annual leave"
                }),
                PropertyPaneTextField('labelPrevLongLeave', {
                  label: "Label for previous long leave"
                }),
                PropertyPaneTextField('labelUsedPrevLongLeave', {
                  label: "Label for previous use long leave"
                }),
                PropertyPaneTextField('labelPrevBalance', {
                  label: "Label for previous balance leave"
                }),
                PropertyPaneTextField('labelValidPrevLeaveBalance', {
                  label: "Label for valid previous leave"
                }),

                PropertyPaneTextField('labelCurrLeaveBalance', {
                  label: "Label for Current leave balance section"
                }),
                PropertyPaneTextField('labelCurrAnnual', {
                  label: "Label for current annual leave"
                }),
                PropertyPaneTextField('labelUsedCurrAnnual', {
                  label: "Label for current use annual leave"
                }),
                PropertyPaneTextField('labelCurrLongLeave', {
                  label: "Label for current long leave"
                }),
                PropertyPaneTextField('labelUsedCurrLongLeave', {
                  label: "Label for current use long leave"
                }),
                PropertyPaneTextField('labelCurrBalance', {
                  label: "Label for current balance leave"
                }),
                PropertyPaneTextField('labelValidCurrLeaveBalance', {
                  label: "Label for valid current leave"
                }),
                PropertyFieldFilePicker('filePickerBackground', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerBackground,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Browse Background Image",                  
                })
              ]
            }
          ]
        }
      ]
    };
  }

  protected onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnpSetup({
        spfxContext: this.context
      });
    });
  }

  /**
   * Handles clicking the Configure button in the placeholder
   */
   private _onConfigure = (): void => {
    // open the property pane to let the user configure the web part
    this.context.propertyPane.open();
  }
  
  /**
   * Check if the web part has been configured
   */
   private _needsConfiguration(): boolean {
    return (this.properties.listid == "" || typeof(this.properties.listid) == "undefined");
  }
}
