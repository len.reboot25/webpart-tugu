declare interface ILeaveBalanceTuguInsuranceWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  LoadingSpinnerLabel: string;
  ErrorLabel: "Error";
  WarningLabel: "Warning";
}

declare module 'LeaveBalanceTuguInsuranceWebPartStrings' {
  const strings: ILeaveBalanceTuguInsuranceWebPartStrings;
  export = strings;
}
