import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'TuguInsuranceUserProfileWebPartStrings';
import TuguInsuranceUserProfile from './components/UserProfileDirectory/TuguInsuranceUserProfile';
import { ITuguInsuranceUserProfileProps } from './components/UserProfileDirectory/ITuguInsuranceUserProfileProps';

import { IPeopleCardProps } from './components/PeopleCard/PeopleCardProps';
import SPFxPeopleCard from './components/PeopleCard/PeopleCard';
import { PersonaSize, PersonaInitialsColor } from 'office-ui-fabric-react';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

import { setup as pnpSetup } from '@pnp/common';
import * as _ from 'lodash';

/**
 * use this props if use folder user profile directory as element
 */
export interface ITuguInsuranceUserProfileWebPartProps {
  title: string;
  filePickerBackground: IFilePickerResult;
}

export default class TuguInsuranceUserProfileWebPart extends BaseClientSideWebPart<IPeopleCardProps> {
  public personaDetail(){
    return React.createElement('Div',{}, React.createElement('span',{},'detail-1'),
        React.createElement('span',{},'detail-2'));
  }

  public render(): void {
    const element: React.ReactElement<IPeopleCardProps> = React.createElement(
      SPFxPeopleCard, {  
        primaryText: this.context.pageContext.user.displayName,
        email: this.context.pageContext.user.email ? this.context.pageContext.user.email : this.context.pageContext.user.loginName,
        serviceScope: this.context.serviceScope,
        class: 'persona-card',
        size: PersonaSize.size100,
        initialsColor: PersonaInitialsColor.darkBlue,
        context: this.context,
        webUrl: this.context.pageContext.web.absoluteUrl,
        spHttpClient: this.context.spHttpClient,
        //moreDetail: this.personaDetail(), /* pass react element */
        //moreDetail: '<div>detail1 <br/> detail2</div>', /* pass html string */
        onCardOpenCallback: ()=>{
          //console.log('WebPart','on card open callaback');
        },
        onCardCloseCallback: ()=>{
          //console.log('WebPart','on card close callaback');
        },
        filePickerBackground: this.properties.filePickerBackground
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  /*
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  */

  protected getLocaleId() : string {
    return this.context.pageContext.cultureInfo.currentUICultureName;
  }

  protected onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnpSetup({
        spfxContext: this.context
      });
    });
  }
    

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: ""
          },
          groups: [
            {
              groupName: 'Personalize Settings',
              groupFields: [
                PropertyFieldFilePicker('filePickerBackground', {
                  context: this.context,
                  filePickerResult: this.properties.filePickerBackground,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e;  },
                  onChanged: (e: IFilePickerResult) => { console.log(e); this.properties.filePickerBackground = e; },
                  key: "filePickerId",
                  buttonLabel: "File Picker",
                  label: "Background Image",                  
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
