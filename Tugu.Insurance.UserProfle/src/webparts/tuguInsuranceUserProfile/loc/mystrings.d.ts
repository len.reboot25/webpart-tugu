declare interface ITuguInsuranceUserProfileWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  ErrorLabel: string;
  LoadingSpinnerLabel: string;
  SkillsLabel: string;
  ProjectsLabel: string;
  CopyEmailLabel: string;
  CopyPhoneLabel: string;
  CopyMobileLabel: string;
}

declare module 'TuguInsuranceUserProfileWebPartStrings' {
  const strings: ITuguInsuranceUserProfileWebPartStrings;
  export = strings;
}
