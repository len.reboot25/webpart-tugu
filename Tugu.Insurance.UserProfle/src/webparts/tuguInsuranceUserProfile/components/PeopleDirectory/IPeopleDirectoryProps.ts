import { IPerson } from "../UserProfileDirectory";

/**
 * Properties for the people component
 */
export interface IPeopleDirectoryProps {
  /**
   * Array of people matching the selected tab or the current search query
   */
   people: IPerson[];
}