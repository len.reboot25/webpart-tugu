import * as React from 'react';
import { IPeopleDirectoryProps } from '.';
import {
  Persona,
  PersonaSize
} from 'office-ui-fabric-react/lib/Persona';
import * as strings from 'TuguInsuranceUserProfileWebPartStrings';
import styles from './PeopleDirectory.module.scss';
import { Callout, DirectionalHint } from 'office-ui-fabric-react/lib/Callout';
import { IPeopleDirectoryState } from './IPeopleDirectoryState';
import { PeopleCallout } from '../PeopleCallout';

export class PeopleDirectory extends React.Component<IPeopleDirectoryProps, IPeopleDirectoryState> {
    constructor(props: IPeopleDirectoryProps) {
      super(props);
  
      this.state = {
        showCallOut: false,
        calloutElement: null,
        person: null
      };
  
      //this._onPersonaClicked = this._onPersonaClicked.bind(this);
      this._onCalloutDismiss = this._onCalloutDismiss.bind(this);
    }
  
    public render(): React.ReactElement<IPeopleDirectoryProps> {
      return (
        <div>
        {this.props.people == null ? 0 : this.props.people.length > 0 &&
          // for each retrieved person, create a persona card with the retrieved
          // information
          //this.props.people.map(p => <Persona primaryText={p.name} secondaryText={p.email} tertiaryText={p.phone} imageUrl={p.photoUrl} imageAlt={p.name} size={PersonaSize.size72} />)
          this.props.people.map((p,i) => {
            const phone: string = p.phone && p.mobile ? `${p.phone}/${p.mobile}`: p.phone ? p.phone: p.mobile;
            // const toggleClassName: string = this.state.toggleClass ? `ms-Icon--ChromeClose ${styles.isClose}` : "ms-Icon--ContactInfo";
            return (
              <div className={styles.persona_card}>
                <Persona primaryText={p.name} secondaryText={p.email} tertiaryText={phone} imageUrl={p.photoUrl} imageAlt={p.name} size={PersonaSize.size72} />
                  <div id={`callout${i}`} onClick={this._onPersonaClicked(i, p)} className={styles.persona}>
                    <i className="ms-Icon ms-Icon--ContactInfo" aria-hidden="true"></i>
                  </div>
                
                { //this.state.showCallOut && this.state.calloutElement === i && (
                  /*<Callout
                    className={this.state.showCallOut ? styles.calloutShow: styles.callout}
                    gapSpace={16}
                    target={`#callout${i}`}
                    isBeakVisible={true}
                    beakWidth={18}
                    setInitialFocus={true}
                    onDismiss={this._onCalloutDismiss}
                    directionalHint={DirectionalHint.rightCenter}
                    doNotLayer={false}
                  >
                    <PeopleCallout person={this.state.person}></PeopleCallout>
                  </Callout> */
                //)
                }
              </div>
            );
          })
        }
      </div>
      );
    }
  
    private _onPersonaClicked = (index, person) => event => {
      this.setState({
        showCallOut: !this.state.showCallOut,
        calloutElement: index,
        person: person
      });
    }
  
    private _onCalloutDismiss = (event) => {
      this.setState({
        showCallOut: false,
      });
    }
  }