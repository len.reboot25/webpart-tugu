import { IPerson } from "../UserProfileDirectory";

export interface IPeopleDirectoryState {
   showCallOut: boolean;
   calloutElement: number;
   person: IPerson;
}