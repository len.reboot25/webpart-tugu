import { IPerson } from ".";

export interface ITuguInsuranceUserProfileState {
  /**
     * True if the component is loading its data, false otherwise
     */
   loading: boolean;
   /**
    * Contains the error message that occurred while loading the data.
    * If no error message occurred, null.
    */
   errorMessage: string;

   /**
     * List of people matching either the currently selected tab or the
     * search query. Empty array if no matching people found.
     */
    people: IPerson[];

    /**
     * detail information 
     */
    employeeDetail: string;
}
