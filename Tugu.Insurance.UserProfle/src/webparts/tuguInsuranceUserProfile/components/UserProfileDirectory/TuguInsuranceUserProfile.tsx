import * as React from 'react';
import styles from './TuguInsuranceUserProfile.module.scss';
import { ITuguInsuranceUserProfileProps } from './ITuguInsuranceUserProfileProps';
import { ITuguInsuranceUserProfileState } from './ITuguInsuranceUserProfileState';
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';
import { IPerson } from '.';
import { ICell } from '.';

import { escape } from '@microsoft/sp-lodash-subset';
import * as strings from 'TuguInsuranceUserProfileWebPartStrings';

import {
  MessageBar,
  MessageBarType
} from 'office-ui-fabric-react/lib/MessageBar';
import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

import { sp } from "@pnp/sp/presets/all";
import "@pnp/sp/webs";

import { IConfigOptions } from '@pnp/common';
import { PeopleDirectory } from '../PeopleDirectory';

// nvm 10.17.0

export default class TuguInsuranceUserProfile extends React.Component<ITuguInsuranceUserProfileProps, ITuguInsuranceUserProfileState> {
  constructor(props: ITuguInsuranceUserProfileProps) {
    super(props);

    this.state = {
      loading: false,
      errorMessage: null,
      people:null,
      employeeDetail: null
    };
  }

  public componentDidMount(): void {  
    this.loadUserProfileDetails();  
  }

  /*Get Current Logged In User*/  
  /*
  public async spLoggedInUserDetails(): Promise<any>{  
    try {  
      const headers: IConfigOptions = {
        headers: {
          Accept: 'application/json;odata=nometadata',
        }
      };

      return await sp.web.currentUser.get();            
    } catch (error) {  
      console.log("Error in spLoggedInUserDetails : " + error);  
      this.setState({
        errorMessage : "and error found get current login user, please contact your technical support"
      })  
    }      
  } 
  */
  private _getValueFromSearchResult(key: string, cells: ICell[]): string {
    for (let i: number = 0; i < cells.length; i++) {
      if (cells[i].Key === key) {
        return cells[i].Value;
      }
    }

    return '';
  }

  private async loadUserProfileDetails():Promise<void>{  

    
    this.setState({
      loading: true,
      errorMessage: null,
      people: null
    });

    try{  
      //let userDetails = await this.spLoggedInUserDetails();
      
      const headers: HeadersInit = new Headers();
      // suppress metadata to minimize the amount of data loaded from SharePoint
      headers.append("accept", "application/json;odata.metadata=none");

      //let query: string = userDetails['UserPrincipalName'];
      let query : string = this.props.context.pageContext.user.loginName;
    
      // retrieve information about people using SharePoint People Search
      // sort results ascending by the last name
      this.props.spHttpClient
        .get(`${this.props.webUrl}/_api/search/query?querytext='${query}'&selectproperties='FirstName,LastName,PreferredName,WorkEmail,PictureURL,WorkPhone,MobilePhone,JobTitle,Department,Skills,PastProjects'&sortlist='LastName:ascending'&sourceid='b09a7990-05ea-4af9-81ef-edfab16c4e31'&rowlimit=500`, SPHttpClient.configurations.v1, {
          headers: headers
        })
        .then((res: SPHttpClientResponse) => {
          return res.json();
        })
        .then((res: any): void => {
          if (res.error) {
            // There was an error loading information about people.
            // Notify the user that loading data is finished and return the
            // error message that occurred
            this.setState({
              loading: false,
              errorMessage: res.error.message
            });
            return;
          }
  
          if (res.PrimaryQueryResult.RelevantResults.TotalRows == 0) {
            // No results were found. Notify the user that loading data is finished
            this.setState({
              loading: false
            });
            return;
          }
          
          // convert the SharePoint People Search results to an array of people
          let people: IPerson[] = res.PrimaryQueryResult.RelevantResults.Table.Rows.map(r => {
            return {
              name: this._getValueFromSearchResult('PreferredName', r.Cells),
              firstName: this._getValueFromSearchResult('FirstName', r.Cells),
              lastName: this._getValueFromSearchResult('LastName', r.Cells),
              phone: this._getValueFromSearchResult('WorkPhone', r.Cells),
              mobile: this._getValueFromSearchResult('MobilePhone', r.Cells),
              email: this._getValueFromSearchResult('WorkEmail', r.Cells),
              photoUrl: `${this.props.webUrl}${"/_layouts/15/userphoto.aspx?size=M&accountname=" + this._getValueFromSearchResult('WorkEmail', r.Cells)}`,
              function: this._getValueFromSearchResult('JobTitle', r.Cells),
              department: this._getValueFromSearchResult('Department', r.Cells),
              skills: this._getValueFromSearchResult('Skills', r.Cells),
              projects: this._getValueFromSearchResult('PastProjects', r.Cells)
            };
          });
          
  
          if (people.length > 0) {
            // notify the user that loading the data is finished and return the loaded information
            this.setState({
              loading: false,
              people: people
            });
          }
          else {
            // People collection could be reduced to zero, so no results
            this.setState({
              loading: false
            });
            return;
          }
        }, (error: any): void => {
          // An error has occurred while loading the data. Notify the user
          // that loading data is finished and return the error message.
          this.setState({
            loading: false,
            errorMessage: error
          });
        })
        .catch((error: any): void => {
          // An exception has occurred while loading the data. Notify the user
          // that loading data is finished and return the exception.
          this.setState({
            loading: false,
            errorMessage: error
          });
        });
      
      //userDetails["UserPrincipalName"]  
    }catch(error){  
      console.log("Error in loadUserDetails : ", error);  
      this.setState({
        errorMessage : "and error found get current login user, please contact your technical support"
      })
    }  
  } 

  public render(): React.ReactElement<ITuguInsuranceUserProfileProps> {
    const { loading, errorMessage } = this.state;

    return (
      <div className={styles.tuguInsuranceUserProfile}>
        {!loading &&
          errorMessage &&
          // if the component is not loading data anymore and an error message
          // has been returned, display the error message to the user
          <MessageBar
            messageBarType={MessageBarType.error}
            isMultiline={false}>{strings.ErrorLabel}: {errorMessage}</MessageBar>
        }

        <WebPartTitle
          displayMode={this.props.displayMode}
          title={this.props.title}
          updateProperty={this.props.onTitleUpdate} />

        {loading &&
          // if the component is loading its data, show the spinner
          <Spinner size={SpinnerSize.large} label={strings.LoadingSpinnerLabel} />
        }

        {!loading &&
          !errorMessage &&
          // if the component is not loading data anymore and no errors have occurred
          // render the list of retrieved people
          <PeopleDirectory
            people={this.state.people} />
        }
        
      </div>
    );
  }
}
