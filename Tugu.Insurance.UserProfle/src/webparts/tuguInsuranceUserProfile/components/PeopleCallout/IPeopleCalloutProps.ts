import { IPerson } from "../UserProfileDirectory";

export interface IPeopleCalloutProps {
  person: IPerson;
}