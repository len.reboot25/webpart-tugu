export interface IPeopleCardState {
    pictureUrl: string;
    personaCard: any;
    employeeDetail: string;
    hobbies: string;
}