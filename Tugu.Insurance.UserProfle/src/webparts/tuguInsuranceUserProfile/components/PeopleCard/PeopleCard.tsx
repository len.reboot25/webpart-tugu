import * as React from "react";
import * as ReactDOM from "react-dom";
import { SPComponentLoader } from "@microsoft/sp-loader";
import styles from './PeopleCard.module.scss';
import { Version, Environment, EnvironmentType, ServiceScope, Log, Text } from "@microsoft/sp-core-library";
import { Label, Persona, PersonaSize, IPersonaProps, PersonaInitialsColor } from "@microsoft/office-ui-fabric-react-bundle";
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';
import { WebPartContext } from "@microsoft/sp-webpart-base";
import "@pnp/sp/profiles";
import { MSGraphClient } from '@microsoft/sp-http'; 
import { sp, UrlFieldFormatType } from "@pnp/sp/presets/all";
import { IPeopleCardProps } from "./PeopleCardProps";
import { IPeopleCardState } from "./PeopleCardState";


const EXP_SOURCE: string = "SPFxPeopleCardComponent";

const MD5_MODULE_ID: string = "8494e7d7-6b99-47b2-a741-59873e42f16f";
const LIVE_PERSONA_COMPONENT_ID: string = "914330ee-2df2-4f6e-a858-30c23a812408";
const DEFAULT_PERSONA_IMG_HASH: string = "7ad602295f8386b7615b582d87bcc294";
const PROFILE_IMAGE_URL: string = '/_layouts/15/userphoto.aspx?size={0}&accountname={1}';

export default class PeopleCard extends React.PureComponent<IPeopleCardProps, IPeopleCardState>{

    constructor(props: any, context: any) {
        super(props, context);

        this.state = {
            pictureUrl: null,
            personaCard: null,
            employeeDetail: null,
            hobbies: null
        };
    }

    public componentDidMount(): any {
        const size = this.getPersonaSize();
        const personaImgUrl = Text.format(PROFILE_IMAGE_URL, size, this.props.email);

        this.getImageBase64(personaImgUrl).then((url: string) => {
            this.getMd5HashForUrl(url).then((newHash)=>{
                Log.info(EXP_SOURCE, `${url} h- ${newHash}`);
                if (newHash !== DEFAULT_PERSONA_IMG_HASH) {
                    this.setState({ pictureUrl: "data:image/png;base64," + url });
                }
            });
        });

        this.getEmployeeDetail();
        
        this.loadSPComponentById(LIVE_PERSONA_COMPONENT_ID).then((sharedLibrary: any) => {
            const livePersonaCard: any = sharedLibrary.LivePersonaCard;
            this.setState({ personaCard: livePersonaCard });
        });
    }

    private getPersonaSize(){
        let size = 'M';
        if(this.props.size <= 3){
            size = 'S';
        }else if(this.props.size <= 6 && this.props.size > 5){
            size = 'M';
        }

        return size;
    }

    private getEmployeeDetail(){
        this.props.context.msGraphClientFactory
        .getClient()
        .then((client: MSGraphClient): void => {
            client.api('/me').get(async (error, user: any, rawResponse?: any) => {
            //console.log(user);
            
            let loginName="i:0#.f|membership|"+user.userPrincipalName;
            const profile = await sp.profiles.getPropertiesFor(loginName);
            
            console.log(profile);
            var _aboutme = typeof(profile.UserProfileProperties[15]['Value']) == undefined || profile.UserProfileProperties[15]['Value'] == null ? "" : profile.UserProfileProperties[15]['Value'];
            //var _Group = typeof(user.jobTitle) == "undefined" || user.jobTitle == null ? "-" : user.jobTitle;
            var _Group = typeof(profile.UserProfileProperties[13]['Value']) == "undefined" || profile.UserProfileProperties[22]['Value'] == null ? "-" : profile.UserProfileProperties[13]['Value'];
            var _phone = typeof(user.mobilePhone) != "undefined" && user.mobilePhone != null ? user.mobilePhone : typeof(profile.UserProfileProperties[58]['Value']) == "undefined" || profile.UserProfileProperties[58]['Value'] == null ? "-" : profile.UserProfileProperties[58]['Value'];
            var _ext = typeof(profile.UserProfileProperties[10]['Value']) == "undefined" || profile.UserProfileProperties[10]['Value'] == null ? "-" : profile.UserProfileProperties[10]['Value'];
            var _mail = typeof(user.mail) == "undefined" || user.mail == null ? "-" : user.mail;
            var _dept = typeof(profile.UserProfileProperties[11]['Value']) == "undefined" || profile.UserProfileProperties[11]['Value'] == null ? "-" : profile.UserProfileProperties[11]['Value'];

            var _html = '<div><br>';
            _html += '<span class='+styles.aboutme+'>'+_aboutme+'</span><br>';
            _html += '<span></span><br>';
            _html += '<span class='+styles.employeeInformation+'><b>Employment Information</b></span><br>';
            _html += '<table class='+styles.detailEmployeeInformation+'>';
            _html += '<tr>';
            _html += '<td  class='+styles.title+'><b><label class='+styles.title+'>Group</label></b></td><td class='+styles.splitter+'>:</td><td class='+styles.value+'><label class='+styles.value+'>'+_Group+'</label></td>';
            _html += '</tr>';
            _html += '<tr>';
            _html += '<td  class='+styles.title+'><b><label class='+styles.title+'>Dept.</label></b></td><td class='+styles.splitter+'>:</td><td class='+styles.value+'><label class='+styles.value+'>'+_dept+'</label></td>';
            _html += '</tr>';
            _html += '<tr>';
            _html += '<td class='+styles.title+'><b><label class='+styles.title+'>Email</label></b></td><td class='+styles.splitter+'>:</td><td class='+styles.value+'><label class='+styles.value+'>'+_mail+'</label></td>';
            _html += '</tr>';
            _html += '<tr>';
            _html += '<td  class='+styles.title+'><b><label class='+styles.title+'>Ext.</label></b></td><td class='+styles.splitter+'>:</td><td class='+styles.value+'><label class='+styles.value+'>'+_ext+'</label></td>';
            _html += '</tr>';
            _html += '<tr>';
            _html += '<td  class='+styles.title+'><b><label class='+styles.title+'>Phone</label></b></td><td class='+styles.splitter+'>:</td><td class='+styles.value+'><label class='+styles.value+'>'+_phone+'</label></td>';
            _html += '</tr>';
            _html += '</table>';
            _html += '</div>';

            this.setState({employeeDetail : _html, hobbies: _aboutme});

            });
        })
    }

    private getMoreDetailElement(){
        if(React.isValidElement(this.state.employeeDetail)){
            return React.createElement('div',
            { 
                className: 'more-persona-details' 
            }, this.state.employeeDetail);
        } else {
            return React.createElement('div',
            { 
                className: 'more-persona-details',
                dangerouslySetInnerHTML: { __html: this.state.employeeDetail} 
            });
        }
    }

    /**
     * Display default OfficeUIFabric Persona card if SPFx LivePersonaCard not loaded
     */
    private defaultContactCard() {
        return React.createElement<IPersonaProps>(Persona, {
            primaryText: this.props.primaryText,
            secondaryText: '',
            tertiaryText: this.props.tertiaryText,
            optionalText: this.props.optionalText,
            imageUrl: this.state.pictureUrl,
            initialsColor: this.props.initialsColor ? this.props.initialsColor : "#808080",
            className: styles.mypersona,
            size: this.props.size,
            imageShouldFadeIn: false,
            imageShouldStartVisible: true,
            styles:{
                primaryText: {display:"flex", justifyContent: "center", wordWrap: "break-word", whiteSpace:"normal", overflow:"visible", textAlign:"center"},
                details: {padding: "0px", textAlign:"left", flexDirection:"column"}
            },
        }, this.getMoreDetailElement());
    }

    /**
     * Configure SPFx LivePersona card from SPFx component loader
     */
    private spfxLiverPersonaCard() {
        return React.createElement(this.state.personaCard, {
            className: styles.people, //'people',
            clientScenario: "PeopleWebPart",
            disableHover: false,
            hostAppPersonaInfo: {
                PersonaType: "User"
            },
            serviceScope: this.props.serviceScope,
            upn: this.props.email,
            onCardOpen: () => {
                if(this.props.onCardOpenCallback){
                    this.props.onCardOpenCallback();
                }
            },
            onCardClose: () => {
                if(this.props.onCardCloseCallback){
                    this.props.onCardCloseCallback();
                }
            }
        }, this.defaultContactCard());
    }

    /**
     * Get MD5Hash for the image url to verify whether user has default image or custom image
     * @param url 
     */
    private getMd5HashForUrl(url: string) {
        return new Promise((resolve, reject) => {
            this.loadSPComponentById(MD5_MODULE_ID).then((library: any) => {
                const md5Hash = library.Md5Hash;
                if (md5Hash) {
                    const convertedHash = md5Hash(url);
                    resolve(convertedHash);
                }
            }).catch((error) => {
                Log.error(EXP_SOURCE, error, this.props.serviceScope);
                resolve(url);
            });
        });
    }

    private getImageBase64(pictureUrl: string) {
        return new Promise((resolve, reject) => {
            let image = new Image();
            image.addEventListener("load", () => {
                let tempCanvas = document.createElement("canvas");
                tempCanvas.width = image.width,
                    tempCanvas.height = image.height,
                    tempCanvas.getContext("2d").drawImage(image, 0, 0);
                let base64Str;
                try {
                    base64Str = tempCanvas.toDataURL("image/png");
                } catch (e) {
                    return "";
                }
                base64Str = base64Str.replace(/^data:image\/png;base64,/, "");
                resolve(base64Str);
            });
            image.src = pictureUrl;
        });
    }

    /**
     * Load SPFx component by id, SPComponentLoader is used to load the SPFx components
     * @param componentId - componentId, guid of the component library
     */
    private loadSPComponentById(componentId: string) {
        return new Promise((resolve, reject) => {
            SPComponentLoader.loadComponentById(componentId).then((component: any) => {
                resolve(component);
            }).catch((error) => {
                Log.error(EXP_SOURCE, error, this.props.serviceScope);
            });
        });
    }

    public render(): JSX.Element {
        
        let backgroundImg = '';
        if(typeof(this.props.filePickerBackground) != "undefined")
            backgroundImg = this.props.filePickerBackground.fileAbsoluteUrl;
                
        return (
            <div className={styles.personaCard} style={{backgroundImage:'url("'+ backgroundImg +'")'}}>
                {
                    this.state.personaCard ? this.spfxLiverPersonaCard() : this.defaultContactCard()
                }
            </div>
        );
    }
}
