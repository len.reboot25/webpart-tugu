import { Label, Persona, PersonaSize, IPersonaProps, PersonaInitialsColor } from "@microsoft/office-ui-fabric-react-bundle";
import { WebPartContext } from "@microsoft/sp-webpart-base";
import { Version, Environment, EnvironmentType, ServiceScope, Log, Text } from "@microsoft/sp-core-library";
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';
import { PropertyFieldFilePicker, IPropertyFieldFilePickerProps, IFilePickerResult } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";

export interface IPeopleCardProps {
    primaryText: string;
    secondaryText?: string;
    tertiaryText?: string;
    optionalText?: string;
    moreDetail?: HTMLElement | string;
    email: string;
    serviceScope: ServiceScope;
    class: string;
    size: PersonaSize;
    initialsColor?: PersonaInitialsColor;
    onCardOpenCallback: Function; 
    onCardCloseCallback: Function;
    context: WebPartContext;
    spHttpClient: SPHttpClient;
    webUrl: string;
    filePickerBackground: IFilePickerResult;
}
