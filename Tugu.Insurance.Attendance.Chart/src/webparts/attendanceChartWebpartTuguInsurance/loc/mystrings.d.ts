declare interface IAttendanceChartWebpartTuguInsuranceWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  LoadingSpinnerLabel: string;
  ErrorLabel: "Error";
  WarningLabel: "Warning";
}

declare module 'AttendanceChartWebpartTuguInsuranceWebPartStrings' {
  const strings: IAttendanceChartWebpartTuguInsuranceWebPartStrings;
  export = strings;
}
