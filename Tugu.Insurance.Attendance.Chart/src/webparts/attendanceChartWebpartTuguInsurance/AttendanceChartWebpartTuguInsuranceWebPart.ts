import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneDropdown,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'AttendanceChartWebpartTuguInsuranceWebPartStrings';
import AttendanceChartWebpartTuguInsurance from './components/Content/AttendanceChartWebpartTuguInsurance';
import { IAttendanceChartWebpartTuguInsuranceProps } from './components/Content/IAttendanceChartWebpartTuguInsuranceProps';
import { PropertyFieldListPicker, PropertyFieldListPickerOrderBy } from '@pnp/spfx-property-controls/lib/PropertyFieldListPicker';
import { PropertyFieldColumnPicker, PropertyFieldColumnPickerOrderBy, IColumnReturnProperty } from '@pnp/spfx-property-controls/lib/PropertyFieldColumnPicker';
import { setup as pnpSetup } from '@pnp/common';
import { PropertyFieldColorPicker, PropertyFieldColorPickerStyle } from '@pnp/spfx-property-controls/lib/PropertyFieldColorPicker';

import { spfi, SPFI, SPFx } from "@pnp/sp";
import { getSP } from "./pnpjsConfig";

export interface IAttendanceChartWebpartTuguInsuranceWebPartProps {
  title: string;
  listidChartByGroup: string;
  keyChartByGroup: string;
  filterChartByGroup: string;
  wfoChartByGroup: string;
  wfhChartByGroup: string;
  wfeChartByGroup: string;
  dinasChartByGroup: string;
  unfitAndWorkChartByGroup: string;
  unfitAndNotWorkChartByGroup: string;
  impKhususChartByGroup: string;
  leaveChartByGroup: string;

  listidChartByPerson: string;
  keyChartByPersonal: string;
  filterChartByPersonal: string;
  wfoChartByPerson: string;
  wfhChartByPerson: string;
  wfeChartByPerson: string;
  dinasChartByPerson: string;
  unfitAndWorkChartByPerson: string;
  unfitAndNotWorkChartByPerson: string;
  impKhususChartByPerson: string;
  leaveChartByPerson: string;
  listidGroup: string;
  keyGroup: string;
  valueGroup: string;

  /**
   * Color
   */
  wfoColor: string;
  wfhColor: string;
  wfeColor: string;
  dinasColor: string;
  unfitAndWorkColor: string;
  unfitAndNoWorkColor: string;
  impKhususColor: string;
  leaveColor: string;

}

export default class AttendanceChartWebpartTuguInsuranceWebPart extends BaseClientSideWebPart<IAttendanceChartWebpartTuguInsuranceWebPartProps> {

  public async render(): Promise<void> {
    const element: React.ReactElement<IAttendanceChartWebpartTuguInsuranceProps> = React.createElement(
      AttendanceChartWebpartTuguInsurance,
      {
        title: this.properties.title,
        needsConfiguration: this._needsConfiguration(),
        configureHandler: this._onConfigure,
        _context: this.context,
        displayMode: this.displayMode,
        onTitleUpdate: (newTitle: string) => {
          // after updating the web part title in the component
          // persist it in web part properties yes
          this.properties.title = newTitle;
        },

        listidChartByGroup: this.properties.listidChartByGroup,
        keyChartByGroup: this.properties.keyChartByGroup,
        filterChartByGroup: this.properties.filterChartByGroup,
        wfoChartByGroup: this.properties.wfoChartByGroup,
        wfhChartByGroup: this.properties.wfhChartByGroup,
        wfeChartByGroup: this.properties.wfeChartByGroup,
        dinasChartByGroup: this.properties.dinasChartByGroup,
        unfitAndWorkChartByGroup: this.properties.unfitAndWorkChartByGroup,
        unfitAndNotWorkChartByGroup: this.properties.unfitAndNotWorkChartByGroup,
        impKhususChartByGroup: this.properties.impKhususChartByGroup,
        leaveChartByGroup: this.properties.leaveChartByGroup,

        listidChartByPerson: this.properties.listidChartByPerson,
        keyChartByPersonal: this.properties.keyChartByPersonal,
        filterChartByPersonal: this.properties.filterChartByPersonal,
        wfoChartByPerson: this.properties.wfoChartByPerson,
        wfhChartByPerson: this.properties.wfhChartByPerson,
        wfeChartByPerson: this.properties.wfeChartByPerson,
        dinasChartByPerson: this.properties.dinasChartByPerson,
        unfitAndWorkChartByPerson: this.properties.unfitAndWorkChartByPerson,
        unfitAndNotWorkChartByPerson: this.properties.unfitAndNotWorkChartByPerson,
        impKhususChartByPerson: this.properties.impKhususChartByPerson,
        leaveChartByPerson: this.properties.leaveChartByPerson,

        listidGroup: this.properties.listidGroup,
        keyGroup: this.properties.keyGroup,
        valueGroup: this.properties.valueGroup,

        wfoColor: this.properties.wfoColor,
        wfhColor: this.properties.wfhColor,
        wfeColor: this.properties.wfeColor,
        dinasColor: this.properties.dinasColor,
        unfitAndWorkColor: this.properties.unfitAndWorkColor,
        unfitAndNoWorkColor: this.properties.unfitAndNoWorkColor,
        impKhususColor: this.properties.impKhususColor,
        leaveColor: this.properties.leaveColor
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  //protected get dataVersion(): Version {
    //return Version.parse('1.0');
  //}

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          displayGroupsAsAccordion: true,
          groups: [
            {
              groupName: "Source Data Chart By Group",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidChartByGroup', {
                  label: 'Select a list',
                  selectedList: this.properties.listidChartByGroup,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyChartByGroup', {
                  label: 'Select columns for Group Code',
                  context: this.context as any,
                  selectedColumn: this.properties.keyChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterChartByGroup', {
                  label: 'Select columns for Filter Period',
                  context: this.context as any,
                  selectedColumn: this.properties.filterChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfoChartByGroup', {
                  label: 'Select columns for WFO',
                  context: this.context as any,
                  selectedColumn: this.properties.wfoChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfhChartByGroup', {
                  label: 'Select columns for WFH',
                  context: this.context as any,
                  selectedColumn: this.properties.wfhChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfeChartByGroup', {
                  label: 'Select columns for WFE',
                  context: this.context as any,
                  selectedColumn: this.properties.wfeChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('dinasChartByGroup', {
                  label: 'Select columns for DINAS',
                  context: this.context as any,
                  selectedColumn: this.properties.dinasChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('unfitAndWorkChartByGroup', {
                  label: 'Select columns for UNFIT & Masih Bekerja',
                  context: this.context as any,
                  selectedColumn: this.properties.unfitAndWorkChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('unfitAndNotWorkChartByGroup', {
                  label: 'Select columns for UNFIT & Tidak Bekerja',
                  context: this.context as any,
                  selectedColumn: this.properties.unfitAndNotWorkChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('impKhususChartByGroup', {
                  label: 'Select columns for IMP Khusus',
                  context: this.context as any,
                  selectedColumn: this.properties.impKhususChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('leaveChartByGroup', {
                  label: 'Select columns for CUTI & LIBUR',
                  context: this.context as any,
                  selectedColumn: this.properties.leaveChartByGroup,
                  listId: this.properties.listidChartByGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
              ]
            }
            ,
            {
              groupName: "Source Data Chart By Personal",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidChartByPerson', {
                  label: 'Select a list',
                  selectedList: this.properties.listidChartByPerson,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyChartByPersonal', {
                  label: 'Select columns for Email Employee',
                  context: this.context as any,
                  selectedColumn: this.properties.keyChartByPersonal,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('filterChartByPersonal', {
                  label: 'Select columns for Filter Period',
                  context: this.context as any,
                  selectedColumn: this.properties.filterChartByPersonal,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfoChartByPerson', {
                  label: 'Select columns for WFO',
                  context: this.context as any,
                  selectedColumn: this.properties.wfoChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfhChartByPerson', {
                  label: 'Select columns for WFH',
                  context: this.context as any,
                  selectedColumn: this.properties.wfhChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('wfeChartByPerson', {
                  label: 'Select columns for WFE',
                  context: this.context as any,
                  selectedColumn: this.properties.wfeChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('dinasChartByPerson', {
                  label: 'Select columns for DINAS',
                  context: this.context as any,
                  selectedColumn: this.properties.dinasChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('unfitAndWorkChartByPerson', {
                  label: 'Select columns for UNFIT & Masih Bekerja',
                  context: this.context as any,
                  selectedColumn: this.properties.unfitAndWorkChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('unfitAndNotWorkChartByPerson', {
                  label: 'Select columns for UNFIT & Tidak Bekerja',
                  context: this.context as any,
                  selectedColumn: this.properties.unfitAndNotWorkChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('impKhususChartByPerson', {
                  label: 'Select columns for IMP Khusus',
                  context: this.context as any,
                  selectedColumn: this.properties.impKhususChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('leaveChartByPerson', {
                  label: 'Select columns for CUTI & LIBUR',
                  context: this.context as any,
                  selectedColumn: this.properties.leaveChartByPerson,
                  listId: this.properties.listidChartByPerson,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
              ]
            }
            ,
            {
              groupName: "Source Data Group",
                isCollapsed: true,
                groupFields: [
                PropertyFieldListPicker('listidGroup', {
                  label: 'Select a list',
                  selectedList: this.properties.listidGroup,
                  includeHidden: false,
                  orderBy: PropertyFieldListPickerOrderBy.Title,
                  disabled: false,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  context: this.context as any,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerFieldId',
                  baseTemplate:0
                }),
                PropertyFieldColumnPicker('keyGroup', {
                  label: 'Select columns for key',
                  context: this.context as any,
                  selectedColumn: this.properties.keyGroup,
                  listId: this.properties.listidGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                }),
                PropertyFieldColumnPicker('valueGroup', {
                  label: 'Select columns for value',
                  context: this.context as any,
                  selectedColumn: this.properties.valueGroup,
                  listId: this.properties.listidGroup,
                  orderBy: PropertyFieldColumnPickerOrderBy.Title,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  key: 'columnPickerFieldId',
                  displayHiddenColumns: false,
                  columnsToExclude:["Content Type","Version","Attachments","Edit","Type","Item Child Count","Folder Child Count","Label setting","Retention label","Retention label Applied","Label applied by","App Created By","App Modified By","Compliance Asset Id","Item is a Record"],
                  columnReturnProperty: IColumnReturnProperty['Internal Name'],    
                })
              ]
            }
            ,
            {
              groupName: "Personalize Settings",
                isCollapsed: true,
                groupFields: [
                PropertyFieldColorPicker('wfoColor', {
                    label: 'WFO Color Chart',
                    selectedColor: this.properties.wfoColor,
                    onPropertyChange: this.onPropertyPaneFieldChanged,
                    properties: this.properties,
                    disabled: false,
                    debounce: 1000,
                    isHidden: false,
                    alphaSliderHidden: false,
                    style: PropertyFieldColorPickerStyle.Inline,
                    iconName: 'Precipitation',
                    key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('wfhColor', {
                  label: 'WFH Color Chart',
                  selectedColor: this.properties.wfhColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('wfeColor', {
                  label: 'WFE Color Chart',
                  selectedColor: this.properties.wfeColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('dinasColor', {
                  label: 'Dinas Color Chart',
                  selectedColor: this.properties.dinasColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('unfitAndWorkColor', {
                  label: 'Unfit and Work Color Chart',
                  selectedColor: this.properties.unfitAndWorkColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('unfitAndNoWorkColor', {
                  label: 'Unfit and No Work Color Chart',
                  selectedColor: this.properties.unfitAndNoWorkColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('impKhususColor', {
                  label: 'IMP Khusus Color Chart',
                  selectedColor: this.properties.impKhususColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldColorPicker('leaveColor', {
                  label: 'Leave Color Chart',
                  selectedColor: this.properties.leaveColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  debounce: 1000,
                  isHidden: false,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Inline,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
              ]
            }
          ]
        }
      ]
    };
  }

  protected onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnpSetup({
        spfxContext: this.context
      });
    });
  }

  /**
   * Handles clicking the Configure button in the placeholder
   */
  private _onConfigure = (): void => {
    // open the property pane to let the user configure the web part
    this.context.propertyPane.open();
  }
  
  /**
   * Check if the web part has been configured
   */
   private _needsConfiguration(): boolean {
    return (this.properties.listidChartByGroup == "" || typeof(this.properties.listidChartByGroup) == "undefined")
    && (this.properties.listidChartByPerson == "" || typeof(this.properties.listidChartByPerson) == "undefined")
    && (this.properties.listidGroup == "" || typeof(this.properties.listidGroup) == "undefined")
    ;
  }
}


