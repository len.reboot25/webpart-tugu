import * as React from 'react';
import styles from './AttendanceChartWebpartTuguInsurance.module.scss';
import { IAttendanceChartWebpartTuguInsuranceProps } from './IAttendanceChartWebpartTuguInsuranceProps';
import { IAttendanceChartWebpartTuguInsuranceState } from './IAttendanceChartWebpartTuguInsuranceState';
import { escape } from '@microsoft/sp-lodash-subset';

import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { spfi, SPFI, SPFx } from "@pnp/sp";
import { getSP } from "../../pnpjsConfig";
import {Dropdown, PrimaryButton, IDropdownOption} from '@fluentui/react';

import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

import {
  MessageBar,
  MessageBarType
} from 'office-ui-fabric-react/lib/MessageBar';

import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";
import * as strings from 'AttendanceChartWebpartTuguInsuranceWebPartStrings';

import { ChartControl, ChartType } from '@pnp/spfx-controls-react/lib/ChartControl';
import { SPPermission } from '@microsoft/sp-page-context'; 
import { IDropdownStyles } from 'office-ui-fabric-react';

/**
 * Pre requisite development
 * NVM Version: 10.17.0
 * Typescript version: @microsoft/rush-stack-compiler-4.2
 * pnp version: 3.6.0
 */

const dropdownStyles: Partial<IDropdownStyles> = {
  dropdown: { width: "30%" },
};

const _month : any = ["January","February","March","April","May","June","July","August","September","October","November","December"];

export default class AttendanceChartWebpartTuguInsurance extends React.Component<IAttendanceChartWebpartTuguInsuranceProps, IAttendanceChartWebpartTuguInsuranceState> {
  private _sp: SPFI;

  constructor(props: IAttendanceChartWebpartTuguInsuranceProps, state : IAttendanceChartWebpartTuguInsuranceState)
  {
    super(props);
    this.state = {
      title: this.props.title,
      loading: true,
      errorMessage: null,
      warningMessage: null,
      content: null,
      groupOptions: [],
      periodOptions: [],
      defaultValueGroup: null,
      defaultValuePeriod: null
    };
  }

  public render(): React.ReactElement<IAttendanceChartWebpartTuguInsuranceProps> {
    let contents: JSX.Element;
    
    if (this.props.needsConfiguration === false) {
      if (this.state.loading) {
        // Component is loading its data. Show spinner to communicate this to the user
        contents = <Spinner size={SpinnerSize.large} label={strings.LoadingSpinnerLabel} />;
      } if(!this.state.loading && this.state.errorMessage) {
        contents = <MessageBar
            messageBarType={MessageBarType.error}
            isMultiline={true}>{strings.ErrorLabel}: {this.state.errorMessage}</MessageBar>
      } if(!this.state.loading && this.state.warningMessage) {
        contents = <MessageBar
          messageBarType={MessageBarType.warning}
          isMultiline={true}>{strings.WarningLabel}: {this.state.warningMessage}</MessageBar>
      }else {
        contents = (
          this.state.content
        );
      }
    }
    
    let _permission = this.props._context.pageContext.web.permissions;
    let canEdit = _permission.hasPermission(SPPermission.manageWeb);
    let _fullYear = String(new Date().getFullYear());
    let _filterPeriod =  _month[new Date().getMonth()].substring(0, 3) + '-' + _fullYear.substring(_fullYear.length - 2);

    return (
      <div className={styles.attendanceChartWebpartTuguInsurance}>
        <WebPartTitle
        displayMode={this.props.displayMode}
        title={this.props.title}
        updateProperty={this.props.onTitleUpdate} 
        />
        <Dropdown
            placeholder="Select period data"
            selectedKey={this.state.defaultValuePeriod}
            defaultSelectedKey={_filterPeriod}
            label="Select Period"
            options={this.state.periodOptions}
            styles={dropdownStyles}
            onChange={this.onPeriodChange}
          />
        {
          canEdit && this.state.groupOptions &&
          <Dropdown
            placeholder="Select data to be display"
            selectedKey={this.state.defaultValueGroup}
            defaultSelectedKey={"--me--"}
            label="Select Group or My Data"
            options={this.state.groupOptions}
            styles={dropdownStyles}
            onChange={this.onDataChange}
          />
        }
        <br />
        {
          this.props.needsConfiguration &&
          
          <Placeholder
              iconName='Edit'
              iconText='Tugu Insurance - Attendance Chart Webpart'
              description='Configure your web part'
              buttonLabel='Configure'
              onConfigure={this.props.configureHandler} />
        }
        {
          !this.props.needsConfiguration &&
          contents
        }
      </div>
    );
  }

  public componentDidUpdate(prevProps, prevState): void {
    
    if(prevState.defaultValuePeriod != this.state.defaultValuePeriod || prevState.defaultValueGroup != this.state.defaultValueGroup){
      this.setState({ loading : true, warningMessage : null, errorMessage : null, content : null});
      let _fullYear = String(new Date().getFullYear());
      let _period = _month[new Date().getMonth()].substring(0, 3) + '-' + _fullYear.substring(_fullYear.length - 2);
      
      let _group = "--me--";
      
      if(this.state.defaultValueGroup != null)
        _group = this.state.defaultValueGroup;
      if(this.state.defaultValuePeriod != null)
        _period = this.state.defaultValuePeriod;

      if(_group == "--me--"){
        this._loadReportPeronal(_period);
        this.forceUpdate();
      } else {
        this._loadReportGroup(_period, _group);
        this.forceUpdate();
      }
    }
  }

  public componentDidMount(): void {
    
    if (!this.props.needsConfiguration) {
      // The web part has been configured. Load the chart
      this._loadOptionsGroup();
      this._loadOptionsPeriod();
      
      let _fullYear = String(new Date().getFullYear());
      let _filterPeriod =  _month[new Date().getMonth()].substring(0, 3) + '-' + _fullYear.substring(_fullYear.length - 2);
      
      this._loadReportPeronal(_filterPeriod);
    }
  }

  private _loadReportPeronal(_filter){
    this.setState({ loading : false });
    try
    {
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidChartByPerson)
      .items
      .select(this.props.keyChartByPersonal, this.props.wfoChartByPerson, this.props.wfhChartByPerson,
        this.props.wfeChartByPerson, this.props.dinasChartByPerson, this.props.unfitAndWorkChartByPerson,
        this.props.unfitAndNotWorkChartByPerson, this.props.impKhususChartByPerson, this.props.leaveChartByPerson
        )
      .top(1).filter(this.props.keyChartByPersonal+" eq '"+this.props._context.pageContext.user.email+"' and "+
      this.props.filterChartByPersonal+ " eq '"+_filter+"'"
      )()
      .then((items: any[]) => {
        if(items.length > 0){
          const data: any = {
            labels:
            [
              'WFO',
              'WFH',
              'WFE',
              'DINAS',
              'UNFIT & MASIH BEKERJA',
              'UNFIT & TIDAK BEKERJA',
              'IMP KHUSUS',
              'CUTI & LIBUR'
            ],
            datasets: [
              {
                data: [
                    items[0][this.props.wfoChartByPerson],
                    items[0][this.props.wfhChartByPerson],
                    items[0][this.props.wfeChartByPerson],
                    items[0][this.props.dinasChartByPerson],
                    items[0][this.props.unfitAndWorkChartByPerson],
                    items[0][this.props.unfitAndNotWorkChartByPerson],
                    items[0][this.props.impKhususChartByPerson],
                    items[0][this.props.leaveChartByPerson]
                ],
                backgroundColor: [
                  (typeof(this.props.wfoColor) == "undefined" || this.props.wfoColor == null) ? 'rgb(255, 99, 132)' : this.props.wfoColor,
                  (typeof(this.props.wfhColor) == "undefined" || this.props.wfhColor == null) ? 'rgb(54, 162, 235)' : this.props.wfhColor,
                  (typeof(this.props.wfeColor) == "undefined" || this.props.wfeColor == null) ? 'rgb(255, 205, 86)' : this.props.wfeColor, 
                  (typeof(this.props.dinasColor) == "undefined" || this.props.dinasColor == null) ? 'rgb(128, 255, 128)' : this.props.dinasColor,
                  (typeof(this.props.unfitAndWorkColor) == "undefined" || this.props.unfitAndWorkColor == null) ? 'rgb(255, 153, 238)' : this.props.unfitAndWorkColor,
                  (typeof(this.props.unfitAndNoWorkColor) == "undefined" || this.props.unfitAndNoWorkColor == null) ? 'rgb(255, 42, 0)' : this.props.unfitAndNoWorkColor,
                  (typeof(this.props.impKhususColor) == "undefined" || this.props.impKhususColor == null) ? 'rgb(119, 0, 179)' : this.props.impKhususColor,
                  (typeof(this.props.leaveColor) == "undefined" || this.props.leaveColor == null) ? 'rgb(0, 102, 85)' : this.props.leaveColor
                ],
                hoverOffset: 4
              }
            ]
          };
          
          // set the options
          const options: any = {
            legend: {
              display: true,
            },
            title: {
              //display: true,
              //text: this.props.title
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  
                  let label = data.datasets[tooltipItem.datasetIndex].label || '';
                  var _value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                  if (label) {
                    label += ': ';
                  }

                  label += _value + "%";
                  return label;
                }
              }
            }
          };
          
          this.setState({ content :  <ChartControl
            type={ChartType.Pie}
            data={data}
            options={options}
          />});
        } else {
          this.setState({ warningMessage : "There is no data" });
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    } 
  }

  private _loadReportGroup(_filterPeriod, _filterGroup){
    this.setState({ loading : false });
    try
    {
      
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidChartByGroup)
      .items
      .select(this.props.keyChartByGroup, this.props.wfoChartByGroup, this.props.wfhChartByGroup,
        this.props.wfeChartByGroup, this.props.dinasChartByGroup, this.props.unfitAndWorkChartByGroup,
        this.props.unfitAndNotWorkChartByGroup, this.props.impKhususChartByGroup, this.props.leaveChartByGroup
        )
      .top(1).filter(this.props.keyChartByGroup+" eq '"+_filterGroup+"' and "+
      this.props.filterChartByGroup+ " eq '"+_filterPeriod+"'"
      )()
      .then((items: any[]) => {
        
        if(items.length > 0){
          const data: any = {
            labels:
            [
              'WFO',
              'WFH',
              'WFE',
              'DINAS',
              'UNFIT & MASIH BEKERJA',
              'UNFIT & TIDAK BEKERJA',
              'IMP KHUSUS',
              'CUTI & LIBUR'
            ],
            datasets: [
              {
                data: [
                    items[0][this.props.wfoChartByGroup],
                    items[0][this.props.wfhChartByGroup],
                    items[0][this.props.wfeChartByGroup],
                    items[0][this.props.dinasChartByGroup],
                    items[0][this.props.unfitAndWorkChartByGroup],
                    items[0][this.props.unfitAndNotWorkChartByGroup],
                    items[0][this.props.impKhususChartByGroup],
                    items[0][this.props.leaveChartByGroup]
                ],
                backgroundColor: [
                  (typeof(this.props.wfoColor) == "undefined" || this.props.wfoColor == null) ? 'rgb(255, 99, 132)' : this.props.wfoColor,
                  (typeof(this.props.wfhColor) == "undefined" || this.props.wfhColor == null) ? 'rgb(54, 162, 235)' : this.props.wfhColor,
                  (typeof(this.props.wfeColor) == "undefined" || this.props.wfeColor == null) ? 'rgb(255, 205, 86)' : this.props.wfeColor, 
                  (typeof(this.props.dinasColor) == "undefined" || this.props.dinasColor == null) ? 'rgb(128, 255, 128)' : this.props.dinasColor,
                  (typeof(this.props.unfitAndWorkColor) == "undefined" || this.props.unfitAndWorkColor == null) ? 'rgb(255, 153, 238)' : this.props.unfitAndWorkColor,
                  (typeof(this.props.unfitAndNoWorkColor) == "undefined" || this.props.unfitAndNoWorkColor == null) ? 'rgb(255, 42, 0)' : this.props.unfitAndNoWorkColor,
                  (typeof(this.props.impKhususColor) == "undefined" || this.props.impKhususColor == null) ? 'rgb(119, 0, 179)' : this.props.impKhususColor,
                  (typeof(this.props.leaveColor) == "undefined" || this.props.leaveColor == null) ? 'rgb(0, 102, 85)' : this.props.leaveColor
                ],
                hoverOffset: 4
              }
            ]
          };
          
          // set the options
          const options: any = {
            legend: {
              display: true,
            },
            title: {
              //display: true,
              //text: this.props.title
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  let label = data.datasets[tooltipItem.datasetIndex].label || '';
                  var _value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                  if (label) {
                    label += ': ';
                  }

                  label += _value + "%";
                  return label;
                }
              }
            }
          };
          
          this.setState({ content :  <ChartControl
            type={ChartType.Pie}
            data={data}
            options={options}
          />});
        } else {
          this.setState({ warningMessage : "There is no data" });
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    } 
  }

  private _loadOptionsGroup(){
    try
    {
      this._sp = getSP("", this.props._context);
      
      this._sp.web.lists
      .getById(this.props.listidGroup)
      .items
      .select(this.props.keyGroup, this.props.valueGroup)
      ()
      .then((items: any[]) => {
        let resultarr = [];
        resultarr.push({
          key:"--me--",
          text:"My Data"})

        if(items.length > 0){          
          for(var i = 0; i < items.length; i++){
            resultarr.push({
              key: items[i][this.props.keyGroup],
              text:items[i][this.props.valueGroup]})
          }                    
          this.setState({groupOptions : resultarr});
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({ errorMessage : 'There is an error found, please contact technical support'});
      })
    }
    catch(error)
    {
      console.log(error);
      this.setState({ errorMessage : 'There is an error found, please contact technical support'});
    }
  }

  private _loadOptionsPeriod(){
    let resultarr = [];
    let i = 1;
    let currentMonth = new Date().getMonth()+1;
    let currentYear = new Date().getFullYear();
    while(i < 4){
      if((currentMonth - 1) == 0){
        currentMonth = 12;
        currentYear = currentYear-1;
      } 
      
      let _filterPeriod =  _month[currentMonth - 1].substring(0, 3) + '-' + String(currentYear).substring(String(currentYear).length - 2);

      resultarr.push({
        key: _filterPeriod,
        text: _month[currentMonth - 1] +" " +currentYear,
      })

      currentMonth = currentMonth - 1;
      i++;
    }

    this.setState({periodOptions : resultarr});
  }

  public onPeriodChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ defaultValuePeriod: item.key as string});
  }

  public onDataChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ defaultValueGroup: item.key as string});
  }
}
