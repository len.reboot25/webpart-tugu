import { WebPartContext } from "@microsoft/sp-webpart-base";
import { DisplayMode } from "@microsoft/sp-core-library";

export interface IAttendanceChartWebpartTuguInsuranceProps {
  title: string;

  needsConfiguration: boolean;

  configureHandler: () => void;
  
  _context: WebPartContext;

  /**
   * Current page display mode. Used to determine if the user should
   * be able to edit the page title or not.
   */
   displayMode: DisplayMode;

   /**
   * Event handler for changing the web part title
   */
  onTitleUpdate: (newTitle: string) => void;

  listidChartByGroup: string;
  keyChartByGroup: string;
  filterChartByGroup: string;
  wfoChartByGroup: string;
  wfhChartByGroup: string;
  wfeChartByGroup: string;
  dinasChartByGroup: string;
  unfitAndWorkChartByGroup: string;
  unfitAndNotWorkChartByGroup: string;
  impKhususChartByGroup: string;
  leaveChartByGroup: string;

  listidChartByPerson: string;
  keyChartByPersonal: string;
  filterChartByPersonal: string;
  wfoChartByPerson: string;
  wfhChartByPerson: string;
  wfeChartByPerson: string;
  dinasChartByPerson: string;
  unfitAndWorkChartByPerson: string;
  unfitAndNotWorkChartByPerson: string;
  impKhususChartByPerson: string;
  leaveChartByPerson: string;

  listidGroup: string;
  keyGroup: string;
  valueGroup: string;

  /**
   * Color
   */
   wfoColor: string;
   wfhColor: string;
   wfeColor: string;
   dinasColor: string;
   unfitAndWorkColor: string;
   unfitAndNoWorkColor: string;
   impKhususColor: string;
   leaveColor: string;

}
